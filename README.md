# 21sh

21sh is the next shell-project after [minishell](https://bitbucket.org/MitrikSicilianTeam/minishell). 21sh supports all functionality, but more advanced input parsing has been implemented.
Also it supports: *pipes, redirections, single, double and back quotes, meta-characters, etc...* It reads (and can change) such environment variables: *ps1, ps2, _, SHLVL*.

This code has been checked thru Valgrind and [PVS-Studio](https://www.viva64.com/en/pvs-studio/)(a great static code analyzer for small and big developments)


## Installing

Clone this repository recusively:
```
git clone --recurse-submodules
```
and
```
make
```

## License
![gplv3-88x31.png](https://www.gnu.org/graphics/gplv3-88x31.png)

This project is licensed under the GNU GPL License, Version 3 - see [LICENSE.md](LICENSE.md) for details.

## Contributing/Issue creating

Raise an issue in the issue tracker, or write me a letter.

## Contacts

* [Bitbucket](https://bitbucket.org/Mitriksicilian/)
* [E-mail](mailto:MitrikSicilian@icloud.com?subject=21sh from Bitbucket)
* MitrikSicilian@icloud.com

## Many thanks

* to [PVS Studio](https://www.viva64.com/en/pvs-studio/) which showed me my mistakes.
* to UNIT Factory, for inspiration to do my best.
* to all UNIT Factory students, who shared their knowledge with me and tested this project.

## UNIT Factory
![UNIT Factory logo](https://unit.ua/static/img/logo.png)

[UNIT Factory](https://uk.wikipedia.org/wiki/UNIT_Factory) was an innovative programming school and a part of [School 42](https://en.wikipedia.org/wiki/42_(school)) in [the heart](https://en.wikipedia.org/wiki/Kyiv) of [Ukraine](https://en.wikipedia.org/wiki/Ukraine).
