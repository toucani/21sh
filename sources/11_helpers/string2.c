/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   string2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/30 16:59:48 by dkovalch          #+#    #+#             */
/*   Updated: 2017/09/28 21:27:17 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "string.h"
#include "grammar.h"

/*
**	Helpers - strings
**	This function return pointer to the next 'word'. 'Word' is something
**	after quotes, or something after spaces(if there are no quotes). It
**	understands different quotes.
**
**	Return value:
**		NULL	-	non-valid input, or there are no more words
**		pointer	-	next word
*/

const char	*str_get_next_word(const char *const input)
{
	size_t		ct;
	const char	*rt;

	ct = 0;
	if (!input || !input[0])
		return (NULL);
	rt = input;
	if (gr_is_qt(rt, 0))
	{
		if (!(rt = str_skip_qts(rt)))
			return (NULL);
	}
	else
		while (rt[ct] && !ft_isspc(rt[ct]) && !gr_is_qt(rt, ct))
			ct++;
	while (rt[ct] && ft_isspc(rt[ct]))
		ct++;
	return ((rt[ct]) ? &(rt[ct]) : NULL);
}

/*
**	Helpers - strings
**	This function counts 'words' in the given string.
**
**	Return value:
**		amount of words
*/

size_t		str_count_words(const char *const input)
{
	size_t		rt;
	const char	*temp;

	temp = input;
	rt = (input && input[0] && !ft_isspc(input[0])) ? 1 : 0;
	while ((temp = str_get_next_word(temp)))
		rt++;
	return (rt);
}

/*
**	Helpers - strings
**	This function splits given string into words, using quotes and spaces
**	as delimiters, and trims every word.
**
**	Return value:
**		NULL-terminated array of words
*/

char		**str_split_into_words(const char *const input)
{
	const size_t	words_am = str_count_words(input);
	size_t			words_ct;
	const char		*word;
	const char		*word_next;
	char			**result;

	word = input;
	words_ct = 0;
	result = (char**)ft_memalloc(sizeof(char*) * (words_am + 1));
	while (words_ct < words_am && word != NULL)
	{
		word_next = str_get_next_word(word);
		result[words_ct] = (word_next) ?
			ft_strsub(word, 0, word_next - word) : ft_strdup(word);
		str_trim(&(result[words_ct++]));
		word = word_next;
	}
	return (result);
}

/*
**	This functions returns number of bytes in multi-byte character, whose
**	first byte is given. Works with UTF-8 encoding. Shouldn't depend on
**	endiannes.
*/

unsigned	str_char_length(const unsigned char c)
{
	if ((c & 0xFC) == 0xFC && BIT_UNSET(c, 1))
		return (6);
	else if ((c & 0xF8) == 0xF8 && BIT_UNSET(c, 2))
		return (5);
	else if ((c & 0xF0) == 0xF0 && BIT_UNSET(c, 3))
		return (4);
	else if ((c & 0xE0) == 0xE0 && BIT_UNSET(c, 4))
		return (3);
	else if ((c & 0xC0) == 0xC0 && BIT_UNSET(c, 5))
		return (2);
	return (1);
}
