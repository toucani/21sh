/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   string_array2.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/03 16:26:45 by dkovalch          #+#    #+#             */
/*   Updated: 2017/09/24 17:01:33 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "string_array.h"

/*
**	Helpers - string array
**	This function sort given string array by lexigraphical order.
**	TODO: new sorting algoritm. Current(bubble sort) is O(n^2).
*/

void	str_arr_sort(const char **arr)
{
	const char	*temp;
	size_t		ct;
	bool		was_sorted;

	if (!arr)
		return ;
	was_sorted = true;
	while (was_sorted)
	{
		ct = 0;
		was_sorted = false;
		while (arr[ct])
		{
			if (ft_strcmp(arr[ct], arr[ct + 1]) > 0)
			{
				temp = arr[ct];
				arr[ct] = arr[ct + 1];
				arr[ct + 1] = temp;
				was_sorted = true;
			}
			ct++;
		}
	}
}
