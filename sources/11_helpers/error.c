/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/08 11:12:21 by dkovalch          #+#    #+#             */
/*   Updated: 2017/09/24 22:41:05 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "error.h"
#include "service.h"
#include "structs_global.h"

/*
**	Error service
**	This function prints an error message to STDERR in the sollowing format:
**		<programm name>: <message 1>: <message 2>\n
**	If NULL is given instead of the second message,
**	only programm name and the first message is printed.
*/

void	err_print(const char *const msg, const char *const msg2)
{
	ft_putstr_fd(g_global->program_name, STDERR_FILENO);
	ft_putstr_fd(": ", STDERR_FILENO);
	ft_putstr_fd(msg, STDERR_FILENO);
	if (msg2)
	{
		ft_putstr_fd(": ", STDERR_FILENO);
		ft_putstr_fd(msg2, STDERR_FILENO);
	}
	ft_putchar_fd('\n', STDERR_FILENO);
}

/*
**	Error service
**	This function prints an error message to STDERR and exits the program.
*/

void	err_print_fatal(const char *const msg, const char *const msg2)
{
	err_print(msg, msg2);
	exit(EXIT_FAILURE);
}

/*
**	Error service
**	This function prints an error message to STDERR.
*/

void	err_print_long(const char *const msg, const char *const msg2,
			const char *const msg3)
{
	ft_putstr_fd(g_global->program_name, STDERR_FILENO);
	ft_putstr_fd(": ", STDERR_FILENO);
	ft_putstr_fd(msg, STDERR_FILENO);
	if (msg2)
	{
		ft_putstr_fd(": ", STDERR_FILENO);
		ft_putstr_fd(msg2, STDERR_FILENO);
	}
	if (msg3)
	{
		ft_putstr_fd(": ", STDERR_FILENO);
		ft_putstr_fd(msg3, STDERR_FILENO);
	}
	ft_putchar_fd('\n', STDERR_FILENO);
}

/*
**	Error service
**	This function checks access rights to the given path.
**
**	Return value:
**		true	-	path exists, and can be accessed
**		false	-	no path is given, or it doesn't exist,
**					or cannot be accessed with given mode
*/

bool	err_can_access(const char *const path, const int mode)
{
	if (!serv_can_access(path, F_OK))
		err_print(ERR_NO_DIR, path);
	else if (!serv_can_access(path, mode))
		err_print(ERR_PER_DEN, path);
	else
		return (true);
	return (false);
}
