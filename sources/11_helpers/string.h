/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   string.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/03 16:07:53 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/03 22:17:40 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef STRING_H
# define STRING_H

/*
**	This file provides string library.
**
**	Should be used:
**	everywhere we need to split string into words.
**
**	Function naming:
**	str_...
*/

# include <stddef.h>
# include <stdbool.h>

/*
**---------------------------------------------------------------------------
**|		Name		|					Description							|
**|-----------------|-------------------------------------------------------|
**|		trim		|	Deletes whitespaces from the beginning and the end.	|
**|		trim_qts	|	Deletes quotations marks from the beginning			|
**|					|		and the end.									|
**|		check_qts	|	Returns true if the the number of quotations marks	|
**|					|		is even. It takes into account different types	|
**|					|		of qt marks, and understands that \" is			|
**|					|		not a qt mark.									|
**|		skip_qts	|	Takes the pointer, and iterates it until the same qt|
**|					|		is found. If qt is not found - returns NULL		|
**|		count_words	|	Counts words in given line.							|
**|					|		Word - is something without whitespaces, or		|
**|					|		something in quotes(", ' , `).					|
**|	get_next_word	|	Returns a pointer to the next non-whitespace char	|
**|					|		after the word.									|
**|	split_into_words|	Returns a pointer to the null terminated array of	|
**|					|		words.											|
**|	str_char_length	|	Returns length of the symbol. Is used in line reader|
**|					|	and key actions to determine if we are working with	|
**|					|	ASCII symbol, or Unicode(UTF-8) symbol, and return	|
**|					|	byte length of the symbol.
**---------------------------------------------------------------------------
*/

void		str_trim(char **input);
void		str_trim_qts(char **input);
bool		str_check_qts(const char *const input);
const char	*str_skip_qts(const char *const input);
const char	*str_get_next_word(const char *const input);
size_t		str_count_words(const char *const input) __attribute__((pure));
char		**str_split_into_words(const char *const input);
unsigned	str_char_length(const unsigned char
				c) __attribute__((const,warn_unused_result));

#endif
