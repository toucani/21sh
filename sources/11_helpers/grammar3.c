/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   grammar3.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/06 20:50:45 by dkovalch          #+#    #+#             */
/*   Updated: 2017/09/23 16:49:53 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "grammar.h"

bool		gr_is_wildcard_or_qestion(const char *const str, const size_t ct)
{
	return (str && (gr_is_wildcard(str, ct) || gr_is_question_mark(str, ct)));
}

bool		gr_is_redirection(const char *const str, const size_t ct)
{
	return (str && !gr_has_esc_char(str, ct)
		&& (str[ct] == '>' || str[ct] == '<')
		&& !gr_is_heredoc(str, ct));
}

bool		gr_is_separator(const char *const str, const size_t ct)
{
	return (str && !gr_has_esc_char(str, ct)
		&& (ft_strnequ(&str[ct], ";", 1)
			|| ft_strnequ(&str[ct], "&&", 2)
			|| ft_strnequ(&str[ct], "||", 2)
			|| gr_is_background(str, ct)
			|| gr_is_pipe(str, ct)));
}

bool		gr_is_pipe(const char *const str, const size_t ct)
{
	return (str && !gr_has_esc_char(str, ct)
		&& str[ct] == '|'
		&& str[ct + 1] != '|');
}

bool		gr_is_background(const char *const str, const size_t ct)
{
	return (str && !gr_has_esc_char(str, ct)
		&& str[ct] == '&'
		&& str[ct + 1] != '&'
		&& (ct == 0 || !gr_is_redirection(str, ct - 1)));
}
