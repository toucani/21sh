/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   string_array.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/27 18:21:46 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/03 22:17:01 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "string_array.h"

/*
**	Helpers - string array
**	This function returns amount of elements in the given array.
*/

size_t				str_arr_length(const char *const *const arr)
{
	size_t	ct;

	ct = 0;
	while (arr && arr[ct])
		ct++;
	return (ct);
}

/*
**	Helpers - string array
**	This function creates a new array, with all elements before or after
**	element with given index. It doesnt copy elements itself, only pointers.
**
**	Return value:
**	array of pointers, to strings in the given array.
*/

const char *const	*str_arr_split_array(const char *const *const args,
									const size_t arg_no, const bool before)
{
	const char *const	*rt;
	size_t				size;
	size_t				start;

	if (before)
	{
		start = 0;
		size = arg_no;
	}
	else
	{
		start = arg_no + 1;
		size = str_arr_length(args);
		if (size < start)
			return (NULL);
		size -= arg_no + 1;
	}
	rt = (const char *const *)ft_memalloc((sizeof(char*) * (size + 1)));
	ft_memmove((void*)rt, (void*)&(args[start]), sizeof(char *) * size);
	return (rt);
}

/*
**	Helpers - string array
**	This function creates a new array, with all elements from the array of
**	string arrays given as arguments.
**	It doesnt copy elements itself, only pointers.
**
**	Return value:
**	array of pointers, to strings from all arrays.
*/

const char *const	*str_arr_concat_arr_arrays(
				const char *const *const *const args, const size_t num)
{
	size_t				ct;
	size_t				len;
	const char *const	*total;

	ct = 0;
	len = 0;
	while (ct < num)
		len += str_arr_length(args[ct++]);
	total = (const char *const*)ft_memalloc(sizeof(char*) * (len + 1));
	ct = 0;
	while (ct < num)
	{
		ft_memmove((void*)&(total[str_arr_length(total)]), (void*)args[ct],
			str_arr_length(args[ct]) * sizeof(char*));
		ct++;
	}
	return (total);
}

/*
**	Helpers - string array
**	This function creates a new array, with all elements from 2 arrays of
**	string arrays given as arguments. It can delete previous arrays.
**	It doesnt copy elements itself, only pointers.
**
**	Return value:
**	array of pointers, to strings from both arrays.
*/

const char *const	*str_arr_concat_arrays(const char *const *const arr1,
						const char *const *const arr2, const bool delete)
{
	const char *const	*total;

	total = (const char *const*)ft_memalloc(sizeof(char*) *
		(str_arr_length(arr1) + str_arr_length(arr2) + 1));
	ft_memmove((void*)&(total[str_arr_length(total)]), (void*)arr1,
		str_arr_length(arr1) * sizeof(char*));
	ft_memmove((void*)&(total[str_arr_length(total)]), (void*)arr2,
		str_arr_length(arr2) * sizeof(char*));
	if (delete)
	{
		ft_memdel((void**)&(arr1));
		ft_memdel((void**)&(arr2));
	}
	return (total);
}

/*
**	Helpers - string array
**	This function creates a new array, without the element with given index.
**	It deletes the element, and copies pointer into a new array.
**
**	Return value:
**	array of pointers, to strings, without string with given index.
*/

void				str_arr_remove_one(char *const **const args,
										const size_t arg_no)
{
	const char *const	*old_arg[2];

	old_arg[0] = str_arr_split_array(
		(const char *const *const)*args, arg_no, true);
	old_arg[1] = str_arr_split_array(
		(const char *const *const)*args, arg_no, false);
	ft_strdel((char**)&((*args)[arg_no]));
	ft_memdel((void**)args);
	*args = (char**)str_arr_concat_arr_arrays(
		(const char *const *const *const)&old_arg, 2);
	ft_memdel((void**)&(old_arg[0]));
	ft_memdel((void**)&(old_arg[1]));
}
