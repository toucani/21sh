/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   string_array.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/03 16:11:00 by dkovalch          #+#    #+#             */
/*   Updated: 2017/09/23 12:06:28 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef STRING_ARRAY_H
# define STRING_ARRAY_H

/*
**	This file provides string library.
**
**	Should be used:
**	everywhere we need to split string into words.
**
**	Function naming:
**	str_arr_...
*/

# include <inttypes.h>

/*
**---------------------------------------------------------------------------
**|			Name		|				Description							|
**|---------------------|---------------------------------------------------|
**|		length			|	Returns the length of the array					|
**|		split_array		|	Returns a part of the array before or after		|
**|						|		the given position							|
**|		concat_arrays	|	Returns concateneted array from					|
**|						|		several given arrays						|
**|		concat_arrays	|	Returns concateneted array from 2 given			|
**|						|		arrays										|
**|		remove_one		|	Returns an array with the element at the given	|
**|						|		position removed							|
**---------------------------------------------------------------------------
*/

size_t				str_arr_length(const char *const *const arr);
const char *const	*str_arr_split_array(const char *const *const args,
									const size_t arg_no, const bool before);
const char *const	*str_arr_concat_arr_arrays(
						const char *const *const *const args, const size_t num);
const char *const	*str_arr_concat_arrays(const char *const *const arr1,
						const char *const *const arr2, const bool delete);
void				str_arr_remove_one(char *const **const args,
										const size_t arg_no);
void				str_arr_sort(const char **arr);

#endif
