/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   grammar.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/03 15:53:11 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/03 22:06:44 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GRAMMAR_H
# define GRAMMAR_H

/*
**	This file provides grammar library.
**
**	Should be used:
**	everywhere grammar(symbol) checks are needed.
**
**	Function naming:
**	gr_...
*/

# include <stdbool.h>
# include <stddef.h>
# include <inttypes.h>

/*
**	Following functions pretty much are the 'grammar' part
**	of lexer and parser section of this program. It heavily relies on them.
**
**	-----------------------------------------------------------------
**	|	Name			|	returns TRUE if given symbol is			|
**	|-------------------|-------------------------------------------|
**	|		qt			|		any quotation mark					|
**	|		dqt			|		double quotation mark				|
**	|		sqt			|		single quotation mark				|
**	|		bqt			|		back quotation mark					|
**	|		var			|		start of variable name				|
**	|		bsl			|		back slash							|
**	|		pipe		|		pipe: |								|
**	|		home		|		home symbol							|
**	|	heredoc			|		heredoc or herestring (<< or <<<)	|
**	|	wildcard		|		wildcard							|
**	|	separator		|		separator(; || &&)					|
**	|	background		|		&, not in '>&2', or &&				|
**	|	redirection		|		redirection or heredoc				|
**	|	has_esc_char	|		if prevous symbol is back slash		|
**	|	question_mark	|		question mark						|
**	|	wld_or_qt_mark	|		wildcard or question mark			|
**	-----------------------------------------------------------------
*/

bool	gr_is_qt(const char *const str, const size_t
				ct) __attribute__((pure,warn_unused_result));
bool	gr_is_dqt(const char *const str, const size_t
				ct) __attribute__((pure,warn_unused_result));
bool	gr_is_sqt(const char *const str, const size_t
				ct) __attribute__((pure,warn_unused_result));
bool	gr_is_bqt(const char *const str, const size_t
				ct) __attribute__((pure,warn_unused_result));
bool	gr_is_var(const char *const str, const size_t
				ct) __attribute__((pure,warn_unused_result));
bool	gr_is_bsl(const char *const str, const size_t
				ct) __attribute__((pure,warn_unused_result));
bool	gr_is_home(const char *const str, const size_t
				ct) __attribute__((pure,warn_unused_result));
bool	gr_is_pipe(const char *const str, const size_t
				ct) __attribute__((pure,warn_unused_result));
bool	gr_is_heredoc(const char *const str, const size_t
				ct) __attribute__((pure,warn_unused_result));
bool	gr_is_wildcard(const char *const str, const size_t
				ct) __attribute__((pure,warn_unused_result));
bool	gr_is_separator(const char *const str, const size_t
				ct) __attribute__((pure,warn_unused_result));
bool	gr_has_esc_char(const char *const str, const size_t
				ct) __attribute__((pure,warn_unused_result));
bool	gr_is_background(const char *const str, const size_t
				ct) __attribute__((pure,warn_unused_result));
bool	gr_is_redirection(const char *const str, const size_t
				ct) __attribute__((pure,warn_unused_result));
bool	gr_is_question_mark(const char *const str, const size_t
				ct) __attribute__((pure,warn_unused_result));
bool	gr_is_wildcard_or_qestion(const char *const str, const size_t
				ct) __attribute__((pure,warn_unused_result));

#endif
