/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   string.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/30 16:59:48 by dkovalch          #+#    #+#             */
/*   Updated: 2017/09/23 12:07:06 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "string.h"
#include "grammar.h"

/*
**	Helpers - strings
**	This function iterates over the given string, and returns pointer to the
**	next character after closing quote. It understands single, double and back
**	quotes as different ones.
**
**	Return value:
**		NULL	-	non-valid input, or there is no closing quote.
**		pointer	-	next symbol, after right closing quote
**
**	Examples:
**		Input:		[ "many words" ]
**		Output:					  ^		-	pointer to '\0'
**
**		Input:		[ "many words ]
**		Output:									NULL
**
**		Input:		[ "many 'words'"another word ]
**		Output:						^	-	pointer to 'a' in 'another'
*/

const char	*str_skip_qts(const char *const input)
{
	size_t		ct;
	const char	*qt;

	if (!input || !(*input) || !gr_is_qt(input, 0))
		return (NULL);
	ct = 1;
	qt = input;
	while (input[ct] && !(input[ct] == *qt && gr_is_qt(input, ct)))
		ct++;
	if (input[ct])
		return (&(input[ct + 1]));
	return (NULL);
}

/*
**	Helpers - strings
**	This function check if we have even amount of quotes. It understands single,
**	double and back quotes as different ones.
**
**	Return value:
**		true	-	amount is even
**		false	-	amount is odd
*/

bool		str_check_qts(const char *const input)
{
	size_t		ct;
	const char	*line;
	const char	*temp;

	ct = 0;
	line = input;
	while (line[ct])
	{
		while (line[ct] && !gr_is_qt(line, ct))
			ct++;
		if (line[ct] && gr_is_qt(line, ct))
		{
			if (!(temp = str_skip_qts(&(line[ct]))))
				return (false);
			ct += ABS(temp - &(line[ct]));
		}
	}
	return (true);
}

/*
**	Helpers - strings
**	This function removes trailing spaces(if any) from the given string.
*/

void		str_trim(char **input)
{
	char *result;

	result = ft_strtrim(*input);
	ft_strdel(input);
	*input = result;
}

/*
**	Helpers - strings
**	This function removes quotations marks(is any) from the given string.
*/

void		str_trim_qts(char **input)
{
	char *result;

	if (input && *input && gr_is_qt(*input, 0) && ft_strlen(*input) >= 2)
	{
		result = ft_strsub(*input, 1, ft_strlen(*input) - 2);
		ft_strdel(input);
		*input = result;
	}
}
