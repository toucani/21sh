/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   grammar.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/30 15:08:37 by dkovalch          #+#    #+#             */
/*   Updated: 2017/09/09 22:22:33 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "grammar.h"

bool		gr_has_esc_char(const char *const str, const size_t ct)
{
	return (ct > 0 && str && gr_is_bsl(str, ct - 1));
}

bool		gr_is_dqt(const char *const str, const size_t ct)
{
	return (str && !gr_has_esc_char(str, ct) && str[ct] == '"');
}

bool		gr_is_bqt(const char *const str, const size_t ct)
{
	return (str && !gr_has_esc_char(str, ct) && str[ct] == '`');
}

bool		gr_is_sqt(const char *const str, const size_t ct)
{
	return (str && !gr_has_esc_char(str, ct) && str[ct] == '\'');
}

bool		gr_is_qt(const char *const str, const size_t ct)
{
	return (str
		&& (gr_is_dqt(str, ct) || gr_is_sqt(str, ct) || gr_is_bqt(str, ct)));
}
