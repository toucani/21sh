/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   grammar2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/06 20:50:45 by dkovalch          #+#    #+#             */
/*   Updated: 2017/09/09 22:21:54 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "grammar.h"

bool		gr_is_bsl(const char *const str, const size_t ct)
{
	return (str && str[ct] == '\\');
}

bool		gr_is_var(const char *const str, const size_t ct)
{
	return (str && !gr_has_esc_char(str, ct) && str[ct] == '$');
}

bool		gr_is_home(const char *const str, const size_t ct)
{
	return (str && !gr_has_esc_char(str, ct) && str[ct] == '~');
}

bool		gr_is_wildcard(const char *const str, const size_t ct)
{
	return (str && !gr_has_esc_char(str, ct) && str[ct] == '*');
}

bool		gr_is_question_mark(const char *const str, const size_t ct)
{
	return (str && !gr_has_esc_char(str, ct) && str[ct] == '?');
}
