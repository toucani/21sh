/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   service.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/26 09:00:21 by dkovalch          #+#    #+#             */
/*   Updated: 2017/09/14 20:38:50 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include <dirent.h>
#include "service.h"
#include "string_array.h"

/*
**	Service - dir reader
**	This function reads the directory and copies the content if array
**	given as an argument is not NULL. It can be used for 2 purposes:
**	to count the amount of elements we need to allocate memory for, and to
**	copy those element into the array.
**
**	Return value:
**		amount of items in the directory
*/

static size_t	get_dir_elements(const char *const path, char **arr)
{
	size_t			ct;
	struct dirent	*dir_elem;
	DIR				*directory;

	ct = 0;
	if ((directory = opendir(path)))
	{
		while ((dir_elem = readdir(directory)))
		{
			if (arr)
				arr[ct] = ft_strdup(dir_elem->d_name);
			ct++;
		}
		closedir(directory);
	}
	return (ct);
}

/*
**	Service - dir reader
**	This function reads the directory and copies the content into allocated
**	array, then it sorts the array.
**
**	Return value:
**		array of sorted string with directories entries.
*/

char			**serv_dir_get_content(const char *const path)
{
	char			**rt;

	if (!serv_can_access(path, R_OK))
		return (NULL);
	rt = (char**)ft_memalloc(sizeof(char*) *
					(get_dir_elements(path, NULL) + 1));
	get_dir_elements(path, rt);
	str_arr_sort((const char**)rt);
	return (rt);
}

/*
**	Service - dir reader
**	This function counts matches of the given name and array
**	of directory entries, skipping entries starting from "." unless the
**	name itself starts from ".".
**
**	Return value:
**		amount of matches
*/

size_t			serv_dir_count_matches(const char *const name,
						const char *const *const dir_elements)
{
	size_t	ct;
	size_t	match_amount;

	ct = 0;
	match_amount = 0;
	while (dir_elements[ct])
	{
		if ((name[0] == '.' || dir_elements[ct][0] != '.') &&
			ft_strmatch(name, dir_elements[ct]))
			match_amount++;
		ct++;
	}
	return (match_amount);
}

/*
**	Service - dir reader
**	This function returns an array of matches to the given name and path,
**	skipping entries starting from "." unless the name itself starts from ".".
**
**	Return value:
**		array of matches
*/

char			**serv_dir_get_matches(const char *const path,
						const char *const name)
{
	size_t				ct;
	char				**matches;
	size_t				matches_amount;
	size_t				matches_counter;
	char *const *const	content = serv_dir_get_content(path);

	ct = 0;
	matches_counter = 0;
	matches_amount = serv_dir_count_matches(name, (const char**)content);
	matches = (char**)ft_memalloc(sizeof(char*) * (matches_amount + 1));
	while (content[ct] != NULL)
	{
		if (matches && (name[0] == '.' || content[ct][0] != '.') &&
			ft_strmatch(name, content[ct]))
			matches[matches_counter++] = ft_strdup(content[ct]);
		ft_strdel((char**)&(content[ct]));
		ct++;
	}
	ft_memdel((void**)&content);
	return (matches);
}
