/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/03 15:49:25 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/03 22:05:22 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ERROR_H
# define ERROR_H

/*
**	This file provides error texts defines and error printing functions.
**	Also it contains access checking functions.
**
**	Should be used:
**	everywhere error printitng, or checking access to files is needed.
**
**	Function naming:
**	err_...
*/

# include <stdbool.h>

# define ERR_NO_DIR				"no such file or directory"
# define ERR_PER_DEN			"permission denied"
# define ERR_IS_DIR				"is a directory"
# define ERR_UNK_VAR			"unknown variable"
# define ERR_NO_VAR(var)		"cannot find " #var
# define ERR_CHECK_VAR			"check environment variables"
# define ERR_UNK_OPT			"unknown option"
# define ERR_FEW_ARGS			"too few arguments"
# define ERR_MANY_ARGS			"too many arguments"
# define ERR_PARSE				"parse error"
# define ERR_PARSE_AT			"parse error near"
# define ERR_UNEXP_TOKEN		"unexpected token"
# define ERR_BAD_FILED			"bad file descriptor"
# define ERR_CMD_NOT_FOUND		"command not found"
# define ERR_FILENAME_TOO_LONG	"filename is too long"
# define ERR_FILEPATH_TOO_LONG	"path is too long"
# define ERR_REG_SIGNAL			"error registering signals"
# define ERR_INIT_TERMINAL		"error initializing terminal"
# define ERR_CRT_PIPE			"error creating a pipe"
# define ERR_BRK_PIPE			"broken pipe"

void	err_print(const char *const msg, const char *const msg2);
void	err_print_long(const char *const msg,
			const char *const msg2, const char *const msg3);
void	err_print_fatal(const char *const msg, const char *const msg2);
bool	err_can_access(const char *const path, const int mode);

#endif
