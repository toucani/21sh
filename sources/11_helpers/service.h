/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   service.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/03 16:30:48 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/03 22:15:26 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SERVICE_H
# define SERVICE_H

/*
**	This file provides all service functions, which do not belong
**	to other parts of the program(grammar, string, etc...)
**
**	Should be used:
**	everywhere we need these
**
**	Function naming:
**	serv_...
*/

# include <stddef.h>

typedef struct s_env_var	t_env_var;

char	**serv_dir_get_content(const char *const path);
size_t	serv_dir_count_matches(const char *const name,
				const char *const *const dir_elements);
char	**serv_dir_get_matches(const char *const path, const char *const name);
bool	serv_env_is_internal(t_env_var *element);
bool	serv_can_access(const char *const path, const int mode);
bool	serv_can_write(const char *const path_in, const int mode);
bool	serv_is_dir(const char *const path);
char	*serv_status_get_string(const int status);

#endif
