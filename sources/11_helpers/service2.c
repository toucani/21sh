/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   service2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/09 15:40:45 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/03 22:15:36 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "defines.h"
#include <sys/stat.h>
#include <string.h>
#include "structs_environment.h"

/*
**	Service - variables check
**	Checks whether the variable is internal element of the 21sh, and
**	it doesn't need to be send into a child programm
*/

bool	serv_env_is_internal(t_env_var *element)
{
	return (ft_strequ(element->name, DEFAULT_PROMPT_VAR) ||
		ft_strequ(element->name, DEFAULT_PROMPT_CLR_VAR));
}

/*
**	Service - access check
**	This function checks access rights to the given path.
**
**	Return value:
**		true	-	path can be accessed with given mode
**		false	-	no path is given, or it cannot be accessed with given mode
*/

bool	serv_can_access(const char *const path, const int mode)
{
	if (path)
		return (access(path, mode) == 0);
	return (false);
}

/*
**	Service - access check
**	This function checks if the given path point to a directory.
**
**	Return value:
**		true	-	path exists, and is a directory
**		false	-	no path is given, or it doesn't exist,
**					or is not a directory
*/

bool	serv_is_dir(const char *const path)
{
	struct stat sb;

	return (path && serv_can_access(path, F_OK) && stat(path, &sb) == 0
		&& S_ISDIR(sb.st_mode));
}

/*
**	Service - access check
**	This function checks access rights to the given path.
**	Checks if the path_in can be created (presumably for writing)
**	This funtions should be used if we should check:
**		whether the path exists and can be writable, or(if it doesn't exist)
**		whether we can access sub_path.
**
**	Return value:
**		true	-	path can be accessed with given mode
**		false	-	no path is given, or sub-path cannot be
**					accessed with given mode, or it cannot be
**					accessed with given mode
*/

bool	serv_can_write(const char *const path_in, const int mode)
{
	char	*path;
	bool	result;

	path = NULL;
	if (!serv_can_access(path_in, F_OK) && (mode & W_OK))
	{
		if (ft_strrchr(path_in, '/'))
		{
			path = ft_strdup(path_in);
			*(ft_strrchr(path, '/')) = '\0';
		}
		else
			path = ft_strdup(".");
	}
	result = serv_can_access(path ? path : path_in, mode);
	ft_strdel(&path);
	return (result);
}

/*
**	Service - string genrating
**	This function generates appropriate string, from given
**	process exit status.
*/

char	*serv_status_get_string(const int status)
{
	char	*temp;
	char	*message;

	message = NULL;
	if (WIFEXITED(status))
	{
		message = ft_strdup("Exited: ");
		temp = ft_itoa(WEXITSTATUS(status));
		message = ft_strjoin_ultimate(&message, &temp);
	}
	else if (WIFSIGNALED(status))
	{
		message = ft_strjoin_f("Signaled: ", strsignal(WTERMSIG(status)),
			WCOREDUMP(status) ? ", core dumped" : "", "");
	}
	else if (WIFSTOPPED(status))
		message = ft_strjoin("Stopped: ", strsignal(WSTOPSIG(status)));
	else if (WIFCONTINUED(status))
		message = ft_strdup("Continued");
	return (message);
}
