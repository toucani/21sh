/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   help1.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/04 19:40:49 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/03 21:53:26 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "help.h"
#include "libft.h"
#include "defines.h"
#include "ft_printf.h"

/*
**	Main
**	These functions print help message, which is called by runnig the
**	program with --help. Total mess. No comments, sorry.
*/

static void	print_usage(void)
{
	ft_printf("%rUsage:%r\n", "bold", "reset");
	ft_putendl("\t21sh [prompt] [prompt color]\n");
	ft_printf("%rPrompt:%r\n", "bold", "reset");
	ft_printf(
		"\t%rPrompt%r can be any text. Environment variables are accepted.\n",
		"_", "reset");
	ft_putendl("\t\'You are in $PWD\' will be converted into \"");
	ft_putendl("\tYou are in <current directory>\".");
	ft_printf("\tUse %r"DEFAULT_PROMPT_VAR"%r variable, to modify"
	" the prompt after\n\tthe application starts.\n\n", "bold", "reset");
	ft_printf("%rPrompt color:%r\n", "bold", "reset");
}

static void	print_color_modes(void)
{
	ft_putendl("\nWith such modifications:\n");
	ft_printf("\t%r%s%r\t", "bold", "Bold", "reset");
	ft_printf("\t%r%s%r (or _)\n", "_", "Underline", "reset");
	ft_printf("\t%r%s%r", "reverse", "Reversed", "reset");
	ft_printf("\t%r%s%r\n", "dim", "Dimmed", "reset");
	ft_putendl("\nFor example:\n");
	ft_printf("\t%r%s%r\t", "bold green", "Bold green", "reset");
	ft_printf("\t%r%s%r\n", "_ red", "Underlined red", "reset");
	ft_printf("\t%r%s%r\t", "reverse cyan bold", "Reversed bold cyan", "reset");
	ft_printf("%r%s%r\n", "dimmed yellow", "Dimmed yellow", "reset");
}

static void	print_colors(void)
{
	ft_putendl("Such color names can be used:\n");
	ft_printf("\t%r%s%r", "White", "White", "reset");
	ft_printf("\t%r%s%r", "Black", "Black", "reset");
	ft_printf("\t%r%s%r", "Red", "Red", "reset");
	ft_printf("\t%r%s%r\n", "Green", "Green", "reset");
	ft_printf("\t%r%s%r", "Yellow", "Yellow", "reset");
	ft_printf("\t%r%s%r", "Blue", "Blue", "reset");
	ft_printf("\t%r%s%r", "Magenta", "Magenta", "reset");
	ft_printf("\t%r%s%r\n", "Cyan", "Cyan", "reset");
	ft_printf("\t%r%s%r", "light gray", "Light gray", "reset");
	ft_printf("\t%r%s%r\n", "Dark gray", "Dark gray", "reset");
	print_color_modes();
	ft_printf("\n\tUse %r"DEFAULT_PROMPT_CLR_VAR"%r variable, to modify"
	" the prompt color after\n\tthe application starts.\n\n", "bold", "reset");
}

static void	print_buildins(void)
{
	ft_printf("%rBuild-in commands:%r\n", "bold", "reset");
	ft_putendl("\t- cd\n\t- echo\n\t- env\n\t- setenv\n"
		"\t- unsetenv\n\n\t Use [buildin command] -h to read more"
		" about specific command.");
}

void		h_print_help(void)
{
	ft_printf("\n%-30s %12s %30s\n\n",
		DEFAULT_SHELL_NAME, "Help", DEFAULT_SHELL_NAME);
	print_usage();
	print_colors();
	h_print_second_help();
	print_buildins();
	ft_printf("\n\n%37s %10s\n", "Version", VERSION);
	ft_printf("%-24s %12s%12s %24s\n\n", "LSD", "Build on",
		__DATE__, "LSD");
	exit(EXIT_SUCCESS);
}
