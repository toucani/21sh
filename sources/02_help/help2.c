/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   help2.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/04 19:40:49 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/03 21:53:34 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "defines.h"
#include "ft_printf.h"

/*
**	Main
**	These functions print help message, which is called by runnig the
**	program with --help. No comments are availible, it's a mess.
*/

static void	redirection_syntax_print(void)
{
	ft_putendl("\t ________________________________________________________");
	ft_putendl("\t| String |                   Action                      |");
	ft_putendl("\t|--------|-----------------------------------------------|");
	ft_putendl("\t|   >    |  Redirects standard output to the given file  |");
	ft_putendl("\t|  2>    |  Redirects standard error to the given file   |");
	ft_putendl("\t|   >&2  |  Redirects standard output to standard error  |");
	ft_putendl("\t|  1>&2  |  Redirects standard output to standard error  |");
	ft_putendl("\t|  1>&-  |  Closes standard output                       |");
	ft_putendl("\t|   <    |  Redirects the given file to standard input   |");
	ft_putendl("\t|  2<    |  Redirects the given file to standard error   |");
	ft_putendl("\t|________|_______________________________________________|");
	ft_printf("\n\tPlease note, that redirections like: %r%s%r are"
	" unacceptable.\n\tThe highest redirection number is %r3%r.\n", "bold",
	">&2>&3", "reset", "bold", "reset");
	ft_putendl("\n\tRedirections syntax is:");
	ft_printf("\t%4s%r(file descriptor)%r>%r(file name)%r\n", "", "cyan", "red",
		"cyan", "reset");
	ft_printf("\t%4s%r(file descriptor)%r>%r%r&%r(file descriptor)%r\n",
		"", "cyan", "red", "cyan", "red", "cyan", "reset");
	ft_printf("\t%4s%r(file descriptor)%r>%r%r&-%r%r\n",
		"", "cyan", "red", "cyan", "red", "cyan", "reset");
	ft_putendl("\t\t  ^          ^^^      ^");
	ft_putendl("\t\t  |           |       |");
	ft_printf("\t\t%roptional %r mandatory %r optional%r\n\n", "cyan", "red",
		"cyan", "reset");
}

static void	table_print(void)
{
	ft_printf("    | %19s | %-48s |\n", "Tab",
		"Tries to find matches to the current word");
	ft_printf("    | %19s | %-48s |\n", "Delete",
		"Deletes a character to the right of the cursor");
	ft_printf("    | %19s | %-48s |\n", "Backspace",
		"Deletes a character to the left of the cursor");
	ft_printf("    | %19s | %-48s |\n", "Home (Ctrl A)",
		"Moves cursor to the beginning of the command");
	ft_printf("    | %19s | %-48s |\n", "End (Ctrl E)",
		"Moves cursor to the end of the command");
	ft_printf("    | %19s | %-48s |\n", "Up arrow (Ctrl P)",
		"Prints previous command from the history");
	ft_printf("    | %19s | %-48s |\n", "Down arrow (Ctrl N)",
		"Prints next command from the history");
	ft_printf("    | %19s | %-48s |\n", "Alt Left arrow",
		"Moves cursor one word to the left");
	ft_printf("    | %19s | %-48s |\n", "Alt Right arrow",
		"Moves cursor one word to the right");
	ft_printf("    | %19s | %-48s |\n", "Ctrl Up",
		"Moves cursor one line up");
	ft_printf("    | %19s | %-48s |\n", "Ctrl Down",
		"Moves cursor one line down");
	ft_printf("    | %19s | %-48s |\n", "Ctrl U", "Clears current line");
	ft_printf("    | %19s | %-48s |\n", "Ctrl L", "Clears the screen");
}

void		h_print_second_help(void)
{
	ft_printf("%rRedirections syntax:%r\n", "bold", "reset");
	ft_printf("\t"DEFAULT_SHELL_NAME" supports Bourne shell redirection syntax"
	". Here are some\n\tof the forms of supported redirections:\n");
	redirection_syntax_print();
	ft_printf("%rLine-editing controls:%r\n", "bold", "reset");
	ft_printf("\t"DEFAULT_SHELL_NAME" supports such key combinations,"
	" while getting input from the user:\n");
	ft_putendl("     _______________________________________________________"
	"_________________\n    |    Key combination  |                   Action  "
	"                       |\n    |---------------------|----------------"
	"----------------------------------|");
	ft_printf("    | %19s | %-48s |\n", "Left arrow",
		"Moves cursor one symbol to the left");
	ft_printf("    | %19s | %-48s |\n", "Right arrow",
		"Moves cursor one symbol to the right");
	table_print();
	ft_putendl("    |_____________________|_________________________________"
	"_________________|\n");
}
