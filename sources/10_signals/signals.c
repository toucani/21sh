/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   signals.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/13 18:19:09 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/06 14:02:29 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include <errno.h>
#include "error.h"
#include <signal.h>
#include "service.h"
#include <sys/ioctl.h>
#include <sys/wait.h>
#include "structs_global.h"
#include "structs_children.h"
#include "cursor_movements.h"

/*
**	Signals handlers
**	This function is SIGCHLD signal handler. Iterates over the
**	children we have in our "waiting" list, and records a message, if finds
**	the child.
*/

static void	signal_children(const int signo)
{
	pid_t			child;
	int				status;
	t_child_list	*record;

	while ((child = waitpid(-1, &status, WNOHANG | WUNTRACED)) > 0)
	{
		if ((record = st_chl_find(child)))
		{
			record->message = serv_status_get_string(status);
			record->remove = (WIFEXITED(status) || WIFSIGNALED(status));
			record->print = true;
		}
	}
	if (errno == ECHILD && signo == SIGCHLD)
		errno = 0;
}

/*
**	Signals handlers
**	This function is SIGPIPE and SIGWINCH handler.
*/

static void	signal_main(const int signo)
{
	if (signo == SIGPIPE)
		err_print(ERR_BRK_PIPE, NULL);
	else if (signo == SIGWINCH)
	{
		ioctl(g_global->terminal_fd, TIOCGWINSZ, &(g_global->terminal_size));
		if (g_cur_pos)
			g_cur_pos->is_valid = false;
	}
}

/*
**	Main - initialization
**	This functions set signal disposition for the whole program. Must be
**	called only once, from main().
*/

void		m_init_signals(void)
{
	struct sigaction	signals;

	ft_bzero((void*)&signals, sizeof(struct sigaction));
	sigemptyset(&(signals.sa_mask));
	signals.sa_handler = SIG_IGN;
	if (sigaction(SIGTSTP, &signals, NULL) != 0
	|| sigaction(SIGTTIN, &signals, NULL) != 0
	|| sigaction(SIGTTOU, &signals, NULL) != 0
	|| sigaction(SIGURG, &signals, NULL) != 0
	|| sigaction(SIGQUIT, &signals, NULL) != 0
	|| sigaction(SIGINT, &signals, NULL) != 0)
		err_print_fatal(ERR_REG_SIGNAL, NULL);
	signals.sa_handler = signal_main;
	signals.sa_flags = SA_RESTART;
	if (sigaction(SIGPIPE, &signals, NULL) != 0
	|| sigaction(SIGWINCH, &signals, NULL) != 0)
		err_print_fatal(ERR_REG_SIGNAL, NULL);
	signals.sa_handler = signal_children;
	if (sigaction(SIGCHLD, &signals, NULL) != 0)
		err_print_fatal(ERR_REG_SIGNAL, NULL);
}
