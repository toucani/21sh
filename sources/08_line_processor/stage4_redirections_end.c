/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stage4_redirections_end.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/02 16:06:59 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/06 21:33:52 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "error.h"
#include "grammar.h"
#include "line_processor_internal.h"

/*
**	Line processor - stage 4
**	This function checks the given filedescriptor. It must not be greater than
**	the biggest standard filedescriptor(STDIN, STDOUT, STDERR).
**
**	Return value:
**		true	-	success
**		false	-	fail, with error message already printed
*/

static bool	check_fd(const int fd)
{
	char	*temp;

	if (fd < 0 || fd > 2)
	{
		temp = ft_itoa(fd);
		err_print(ERR_BAD_FILED, temp);
		ft_strdel(&temp);
		return (false);
	}
	return (true);
}

/*
**	Line processor - stage 4
**	This function checks and reads what is to the left from
**	the redirection symbol, and puts it into necessary fields
**	of redirection structure.
**
**	Return value:
**		true	-	success
**		false	-	fail, with error message already printed
*/

bool		lp_s4_redirection_get_left(const char *const arg,
										t_redirection *const redir)
{
	size_t	ct;
	int		file_d;

	ct = 0;
	file_d = EMPTY_FILENO;
	while (ft_isdigit(arg[ct]))
		ct++;
	if (ct != 0)
	{
		if (!gr_is_redirection(arg, ct))
		{
			err_print(ERR_PARSE_AT, &(arg[ct]));
			return (false);
		}
		file_d = ft_atoi(arg);
		if (!check_fd(file_d))
			return (false);
		redir->new_fd = file_d;
	}
	else
		redir->new_fd = (arg[0] == '>') ? STDOUT_FILENO : STDIN_FILENO;
	return (true);
}

/*
**	Line processor - stage 4
**	This function checks and reads what is to the right from
**	the redirection symbol, and puts it into necessary fields
**	of redirection structure.
**
**	Return value:
**		true	-	success
**		false	-	fail, with error message already printed
*/

bool		lp_s4_redirection_get_right(t_redirection *const redir,
										const char *redir_str)
{
	int		file_d;

	file_d = EMPTY_FILENO;
	while (redir_str[0] == redir_str[1])
		redir_str++;
	redir_str++;
	if (redir_str[0] == '&')
	{
		if (redir_str[1] == '-')
			redir->flag = CLOSE;
		else if (ft_isdigit(redir_str[1]))
			file_d = ft_atoi(&(redir_str[1]));
		if ((file_d != EMPTY_FILENO && !check_fd(file_d))
		|| (!ft_isdigit(redir_str[1]) && redir_str[1] != '-'))
		{
			err_print(ERR_PARSE_AT, redir_str);
			return (false);
		}
	}
	else if (redir_str[0] != '\0')
		redir->filename = ft_strdup(redir_str);
	redir->old_fd = file_d;
	return (true);
}
