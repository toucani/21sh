/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stage4_wildcard_begin.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/04 21:34:02 by dkovalch          #+#    #+#             */
/*   Updated: 2017/09/23 12:00:39 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "error.h"
#include "grammar.h"
#include "string_array.h"
#include "line_processor_internal.h"

/*
**	Line processor - stage 4
**	This function looks for a wildcard or question mark position.
**
**	Return value:
**		pointer to wildcard or question mark on success, NULL if fails
*/

static char	*wildcard_get_position(const char *str)
{
	size_t	ct;

	ct = 0;
	while (str && str[ct])
		if (gr_is_wildcard_or_qestion(str, ct))
			return ((char*)&(str[ct]));
		else
			ct++;
	return (NULL);
}

/*
**	Line processor - stage 4
**	This function separates path to asterics from the argument.
**
**	Return value:
**		pointer to null-terminated string with the path.
**		NULL if there is no path
*/

static char	*get_path_to_asterics(const char *const arg)
{
	char	*rt;
	char	*temp;

	rt = ft_strdup(arg);
	temp = wildcard_get_position(rt);
	if (temp)
	{
		*temp = 0;
		if (rt[0] != '\0' && ((temp = ft_strrchr(rt, '/'))))
			*((++temp)) = 0;
		else
			ft_strdel(&rt);
	}
	return (rt);
}

/*
**	Line processor - stage 4
**	This function separates name of asterics from the argument.
**
**	Return value:
**		pointer to null-terminated string with the name.
*/

static char	*get_name_of_asterics(const char *const arg, const char *wild_pos)
{
	char	*rt;
	char	*temp;

	while (wild_pos && wild_pos != arg && *wild_pos != '/')
		wild_pos--;
	rt = ft_strdup(wild_pos + ((*wild_pos == '/') ? 1 : 0));
	if ((temp = ft_strchr(rt, '/')))
		*temp = 0;
	return (rt);
}

/*
**	Line processor - stage 4
**	This function splits an argument to path and name parts, checks the access
**	to the path and tries to get corresponding values.
**	Returns the same argument if matches are not found, or the path
**	cannot be reached(and prints error in this case).
**
**	Return value:
**		Array of corresponding matches on success.
**		The same argument on fail.
*/

char		**wildcard_split_and_replace(char *const line,
					const char *const wild_pos)
{
	const char		*path;
	const char		*name;
	const char		*tail;
	char			**rt;

	rt = NULL;
	path = get_path_to_asterics(line);
	name = get_name_of_asterics(line, wild_pos);
	tail = ft_strchr(wild_pos, '/');
	if (err_can_access(path ? path : ".", R_OK))
		rt = lp_s4_replace_wildcard(path, name, tail);
	if (!rt)
	{
		rt = (char**)ft_memalloc(sizeof(char *) * 2);
		rt[0] = line;
	}
	ft_strdel((char**)&path);
	ft_strdel((char**)&name);
	return (rt);
}

/*
**	Line processor - stage 4
**	This function replaces wildcards and/or questions marks with
**	corresponding values, if it is possible.
**	While the argument has an astercics or a question mark - it gets all strings
**	that match the asterics/question mark, and inserts them into
**	the args array in the token.
*/

void		lp_s4_wildcard(t_command *const token, const size_t arg_no)
{
	const char *const	*args[3];
	const char			*last_wld;
	const char			*this_wld;

	this_wld = NULL;
	last_wld = NULL;
	while ((this_wld = wildcard_get_position(token->args[arg_no])) &&
			this_wld != last_wld)
	{
		args[0] = str_arr_split_array(
				(const char *const *const)token->args, arg_no, true);
		args[1] = (const char *const *const)
				wildcard_split_and_replace(token->args[arg_no], this_wld);
		args[2] = str_arr_split_array(
				(const char *const *const)token->args, arg_no, false);
		if (!ft_strequ(args[1][0], token->args[arg_no]))
			ft_memdel((void**)&(token->args[arg_no]));
		ft_memdel((void**)&(token->args));
		token->args = (char**)str_arr_concat_arr_arrays(
				(const char *const *const *const)&args, 3);
		ft_memdel((void**)&(args[0]));
		ft_memdel((void**)&(args[1]));
		ft_memdel((void**)&(args[2]));
		last_wld = this_wld;
	}
}
