/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stage4_redirections_begin.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/02 16:06:59 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/06 22:09:13 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "error.h"
#include "string.h"
#include "grammar.h"
#include "string_array.h"
#include "line_processor_internal.h"

/*
**	Line processor - stage 4
**	This function checks whether the syntax of a redirection is okay.
**
**	Return value:
**		true	-	success
**		false	-	fail, with error message already printed
*/

static bool			check_the_line(const char *const arg)
{
	size_t	ct;

	ct = 0;
	while (arg[ct] != '\0' && arg[ct] == *arg)
		ct++;
	if (!gr_is_redirection(arg, 0) || ft_strnequ(arg, ">>>", 3)
	|| (arg[ct] != '\0'
		&& ((arg[ct] != '&' && !ft_isascii(arg[ct]))
			|| (arg[ct] == '&' && !ft_isdigit(arg[ct + 1])
				&& arg[ct + 1] != '-')
			|| (arg[ct] == '&' && arg[ct + 1] == '-' && arg[ct + 2] != '\0')
			|| ft_strchr(&(arg[ct]), '>')
			|| ft_strchr(&(arg[ct]), '<'))))
	{
		err_print(ERR_PARSE_AT, arg);
		return (false);
	}
	return (true);
}

/*
**	Line processor - stage 4
**	This function looks for a redirection symbol in the given string.
**
**	Return value:
**		pointer to a redirection symbol on success
**		NULL when redirecion is not found
*/

static const char	*find_redirection(const char *const arg)
{
	size_t	ct;

	ct = 0;
	while (arg && arg[ct] && !gr_is_qt(arg, ct))
	{
		if (gr_is_redirection(arg, ct))
			return (&(arg[ct]));
		ct++;
	}
	return (NULL);
}

/*
**	Line processor - stage 4
**	This function parses redirection line.
**
**	Return value:
**		true	-	success
**		false	-	fail, with error message already printed
*/

static bool			create_redirection(t_command *const token,
										const char *const line,
										const char *const redirection)
{
	t_redirection	*current_redir;

	if (!check_the_line(redirection))
		return (false);
	current_redir = st_red_new(token);
	current_redir->flag = (*redirection == '<') ? READ : WRITE;
	if (ft_strnequ(redirection, ">>", 2))
		current_redir->flag = APPEND;
	if (!lp_s4_redirection_get_left(line, current_redir)
	|| !lp_s4_redirection_get_right(current_redir, redirection))
		return (false);
	return (true);
}

/*
**	Line processor - stage 4
**	This function chooses what to do with all these args, if we found
**	redirection symbol.
**
**	Return value:
**		true	-	success
**		false	-	fail, with error message already printed
*/

static bool			choose_right_way(t_command *const token,
									ssize_t *const arg_no,
									t_redirection *current_redir,
									const char *const redirection)
{
	bool	result;

	if (!current_redir || lp_s4_redirection_enough_data(current_redir))
	{
		result = create_redirection(token, token->args[*arg_no], redirection);
		str_arr_remove_one((char *const **const)&(token->args), *arg_no);
		(*arg_no)--;
		return (result);
	}
	else
	{
		err_print(ERR_PARSE_AT, token->args[*arg_no]);
		return (false);
	}
}

/*
**	Line processor - stage 4
**	This function initializes necessary stuff, and decides what to do if
**	we dont have redirection symbol. Second part of this if is in
**	choose_right_way function.
**
**	Return value:
**		true	-	success
**		false	-	fail, with error message already printed
*/

bool				lp_s4_redirection(t_command *const token,
										ssize_t *const arg_no)
{
	bool			no_error;
	const char		*redirection;
	t_redirection	*current_redir;

	current_redir = st_red_get_last(token);
	redirection = find_redirection(token->args[*arg_no]);
	if (redirection)
		no_error = choose_right_way(token, arg_no, current_redir, redirection);
	else
	{
		if (!current_redir || lp_s4_redirection_enough_data(current_redir))
			return (true);
		else
		{
			current_redir->filename = ft_strdup(token->args[*arg_no]);
			str_trim_qts(&(current_redir->filename));
			str_arr_remove_one((char *const **const)&(token->args),
				*arg_no);
			(*arg_no)--;
			return (true);
		}
	}
	return (no_error);
}
