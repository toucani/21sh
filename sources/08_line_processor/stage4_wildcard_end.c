/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stage4_wildcard_end.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/13 13:14:58 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/03 22:00:29 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "grammar.h"
#include "service.h"

/*
**	Line processor - stage 4
**	This function get an array of directory entris,
**	counts matches of the given name and the array,
**	and creates string with path + matched entry + tail.
**
**	Return value:
**		array of matched paths
*/

char			**lp_s4_replace_wildcard(const char *const path,
					const char *const name, const char *const arg_tail)
{
	size_t		ct;
	char		**rt;
	size_t		match_amount;
	const char	**dir_elements;

	rt = NULL;
	dir_elements = (const char**)serv_dir_get_content(path ? path : ".");
	if ((match_amount = serv_dir_count_matches(name, dir_elements)))
	{
		ct = 0;
		rt = (char**)ft_memalloc(sizeof(char*) * (match_amount + 1));
		match_amount = 0;
		while (dir_elements[ct] != NULL)
		{
			if ((name[0] == '.' || dir_elements[ct][0] != '.')
			&& ft_strmatch(name, dir_elements[ct]))
				rt[match_amount++] = ft_strjoin_f(path ? path : "",
					dir_elements[ct], arg_tail, "");
			ft_strdel((char**)&(dir_elements[ct++]));
		}
	}
	ft_memdel((void**)&dir_elements);
	return (rt);
}
