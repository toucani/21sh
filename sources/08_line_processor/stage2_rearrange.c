/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stage2_rearrange.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/30 17:30:17 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/04 20:37:58 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "string.h"
#include "grammar.h"
#include "structs_command.h"

/*
**	Line processor - stage 2
**	This function reconnects parts of piped command to one root.
**	It works with command tokens. It moves current token under the next one,
**	deleting separator node between them.
**
**	Visual representation:
**	Input:
**		A | B | C && D
**
**	Final state:
**		C && D
**		\->B
**		   \->A
**
**	After 1 iteration in this function:
**		B | C && D
**		\->A
**
**	Return values:
**		true - if we shouldn't iterate (we've changed the actual item
**						under the pointer)
**		false - we should move on with our work.
*/

static bool	check_piped_tokens(t_command **head)
{
	t_command	*parent_command;
	t_command	*piped_command;

	if (!head || !(*head) || !((*head)->next) || !((*head)->next->next)
	|| !gr_is_pipe((*head)->next->raw_data, 0))
		return (false);
	piped_command = *head;
	parent_command = (*head)->next->next;
	st_cmd_delete(&((*head)->next));
	*head = parent_command;
	parent_command->piped_commands = piped_command;
	return (true);
}

/*
**	Line processor - stage 2
**	This function sets background flag to the given token and
**	all its piped friends.
*/

static void	check_background(t_command *const token)
{
	t_command	*piped_token;

	token->background = token->next
		&& gr_is_background(token->next->raw_data, 0);
	piped_token = token->piped_commands;
	while (piped_token)
	{
		piped_token->background = token->background;
		piped_token = piped_token->piped_commands;
	}
}

/*
**	Line processor - stage 2
**	This function checks the order of the tokens and rearranges piped tokens.
**	Piped tokens must be arranged from last to the first, in order
**	to be executed right.
**
**	Return value:
**		true	-	success
**		false	-	fail, with error message already printed
*/

void		lp_s2_rearrange_the_list(t_command **head)
{
	t_command	*current;
	bool		reanalyze;

	reanalyze = check_piped_tokens(head);
	while (reanalyze)
		reanalyze = check_piped_tokens(head);
	current = *head;
	while (current)
	{
		reanalyze = false;
		current->separator = gr_is_separator(current->raw_data, 0);
		if (current->separator)
			reanalyze = check_piped_tokens(&(current->next));
		else
			check_background(current);
		if (!reanalyze)
			current = current->next;
	}
}

/*
**	Line processor - stage 2
**	This function splits every token's(including piped ones)
**	raw data into arguments.
*/

void		lp_s2_split_into_args(t_command *head)
{
	t_command	*temp;

	while (head)
	{
		head->args = str_split_into_words(head->raw_data);
		temp = head->piped_commands;
		while (temp)
		{
			temp->args = str_split_into_words(temp->raw_data);
			temp = temp->piped_commands;
		}
		head = head->next;
	}
}
