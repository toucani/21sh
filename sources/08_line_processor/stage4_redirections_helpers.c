/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stage4_redirections_helpers.c                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/02 16:06:59 by dkovalch          #+#    #+#             */
/*   Updated: 2017/09/23 12:10:54 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <unistd.h>
#include "error.h"
#include "service.h"
#include "structs_command.h"

/*
**	Line processor - stage 4
**	This function validates redirections data, and prints an error message if
**	there is any error with it.
**
**	Return value:
**		true	-	success
**		false	-	fail, with error message already printed
*/

bool	lp_s4_redirection_check_data(const t_redirection *const redir)
{
	if (redir->old_fd != EMPTY_FILENO
		&& redir->new_fd != EMPTY_FILENO)
		return (true);
	else if ((redir->old_fd == EMPTY_FILENO && redir->filename))
		return (err_can_access(redir->filename, R_OK));
	else if (redir->new_fd == EMPTY_FILENO && redir->filename)
		return (serv_can_write(redir->filename, W_OK));
	return (false);
}

/*
**	Line processor - stage 4
**	This function checks if we have enough data to
**	actuaaly create a redirection.
**
**	Return value:
**		true	-	success
**		false	-	fail
*/

bool	lp_s4_redirection_enough_data(const t_redirection *const redir)
{
	return ((redir->old_fd != EMPTY_FILENO
		&& redir->new_fd != EMPTY_FILENO)
	|| ((redir->old_fd != EMPTY_FILENO
		|| redir->new_fd != EMPTY_FILENO)
		&& ((redir->filename && redir->filename[0]) || redir->flag == CLOSE)));
}
