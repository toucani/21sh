/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stage4_variables.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/04 21:34:02 by dkovalch          #+#    #+#             */
/*   Updated: 2017/09/24 18:46:19 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "error.h"
#include "grammar.h"
#include "structs_environment.h"

/*
**	Line processor - stage 4
**	This function looks for a variable name, which can consist only of
**	alpha-numeric symbols and _(underscore)
**
**	Return value:
**		pointer to a null terminated string with variable name(without $)
*/

static char	*get_var_name(const char *const str)
{
	size_t	ct;

	ct = (str[0] == '$') ? 1 : 0;
	while (str[ct] && (ft_isalnum(str[ct]) || str[ct] == '_'))
		ct++;
	return (ft_strsub(str, (str[0] == '$') ? 1 : 0, ct - (str[ct] ? 1 : 0)));
}

/*
**	Line processor - stage 4
**	This function replaces variable name in the string, with it's value.
*/

static void	replace_var_to_val(char **arg, size_t ct)
{
	char		*temp;
	char		*var_name;
	t_env_var	*var;

	var_name = get_var_name(&((*arg)[ct]));
	var = st_env_find(var_name);
	if (!var)
		err_print(ERR_UNK_VAR, var_name);
	temp = ft_strnew(ct + 5);
	temp[0] = '\'';
	ft_strncat(temp, *arg, ct);
	temp = ft_strjoin_del_first(&temp, var ? var->data : "");
	temp = ft_strjoin_del_first(&temp, &((*arg)[ct + ft_strlen(var_name) + 1]));
	temp = ft_strjoin_del_first(&temp, "\'");
	ft_strdel(&var_name);
	ft_strdel(arg);
	*arg = temp;
}

/*
**	Line processor - stage 4
**	This function iterates over the string and replaces all variables with
**	their values.
*/

void		lp_s4_replace_vars(char **arg)
{
	size_t	ct;

	ct = 0;
	while ((*arg)[ct])
	{
		if (gr_is_var(*arg, ct))
			replace_var_to_val(arg, ct);
		else
			ct++;
	}
}

/*
**	Line processor - stage 4
**	This function replaces symbol at a given position(ct) with HOME path.
*/

static void	get_home_path(char **arg, size_t ct)
{
	char		*temp;
	t_env_var	*home;

	home = st_env_get_n_check("HOME", getenv("HOME"));
	temp = ft_strsub(*arg, 0, ct);
	temp = ft_strjoin_del_first(&temp, home->data);
	temp = ft_strjoin_del_first(&temp, &((*arg)[ct + 1]));
	ft_strdel(arg);
	*arg = temp;
}

/*
**	Line processor - stage 4
**	This function iterates over the string and replaces all HOME symbols with
**	HOME path value.
*/

void		lp_s4_replace_homes(char **arg)
{
	size_t	ct;

	ct = 0;
	while ((*arg)[ct])
	{
		if (gr_is_home(*arg, ct))
			get_home_path(arg, ct);
		else
			ct++;
	}
}
