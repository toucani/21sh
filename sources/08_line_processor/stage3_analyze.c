/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stage3_analyze.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/02 16:06:59 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/06 14:31:55 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "error.h"
#include "grammar.h"
#include "line_processor_internal.h"

/*
**	Line processor - stage 3
**	This function iteratres over arguments, calling proper functions, depending
**	on the quotation marks presence.
**
**	N.B.: There cannot be a situation when an argument doesn't start
**		from a quotation mark, but contains it somewhere inside, so we don't
**		need to check it.
**
**	Return value:
**		true	-	success
**		false	-	fail, with error message already printed
*/

bool		analyze_every_arg(t_command *token)
{
	ssize_t		ct;
	bool		no_error;

	ct = 0;
	no_error = true;
	while (token && token->args && token->args[ct] && no_error)
	{
		if (gr_is_bqt(token->args[ct], 0))
			lp_s3_process_backqts(&(token->args[ct]));
		no_error = lp_s3_process_argument(token, &ct);
		ct++;
	}
	if (token && token->args)
		token->name = ft_strdup(token->args[0]);
	return (no_error);
}

/*
**	Line processor - stage 3
**	This function calls necessary functions only for command tokens,
**	separator tokens are not analyzed at all, since there is nothing to analyze.
**
**	Return value:
**		true	-	success
**		false	-	fail, with error message already printed
*/

bool		lp_s3_analyze_the_list(t_command *head)
{
	t_command	*piped;

	if (head && head->separator)
	{
		err_print(ERR_PARSE_AT, head->raw_data);
		return (false);
	}
	while (head)
	{
		if (!head->separator && !analyze_every_arg(head))
			return (false);
		if (!head->separator && head->piped_commands)
		{
			piped = head->piped_commands;
			while (piped)
			{
				if (!analyze_every_arg(piped))
					return (false);
				piped = piped->piped_commands;
			}
		}
		head = head->next;
	}
	return (true);
}
