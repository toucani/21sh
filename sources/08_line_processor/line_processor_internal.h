/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   line_processor_internal.h                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/23 11:56:23 by dkovalch          #+#    #+#             */
/*   Updated: 2017/09/23 12:08:53 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LINE_PROCESSOR_INTERNAL_H
# define LINE_PROCESSOR_INTERNAL_H

/*
**	It is internal file which provides all internal functions for
**	line processor.
**
**	Should be used:
**	'line processor' section of the program only.
**
**	Function naming:
**	lp_...
*/

# include "libft.h"
# include <stdbool.h>
# include "structs_command.h"
# include "structs_redirection.h"

void	lp_s2_rearrange_the_list(t_command **head);
void	lp_s2_split_into_args(t_command *head);

bool	lp_s3_analyze_the_list(t_command *head);
void	lp_s3_process_backqts(char **arg);
bool	lp_s3_process_argument(t_command *const token, ssize_t *const arg_no);

void	lp_s4_replace_vars(char **arg);
void	lp_s4_replace_homes(char **arg);
void	lp_s4_replace_bslashes(char **arg);

void	lp_s4_wildcard(t_command *const token, const size_t arg_no);
char	**lp_s4_replace_wildcard(const char *const path,
						const char *const name, const char *const arg_tail);

bool	lp_s4_redirection(t_command *const token, ssize_t *const arg_no);
bool	lp_s4_redirection_check_data(const t_redirection *const redir);
bool	lp_s4_redirection_enough_data(const t_redirection *const redir);
bool	lp_s4_redirection_get_left(const char *const arg,
										t_redirection *const redir);
bool	lp_s4_redirection_get_right(t_redirection *const redir,
										const char *const redir_str);

bool	lp_s4_heredocs(t_command *const token, ssize_t *const arg_no);

#endif
