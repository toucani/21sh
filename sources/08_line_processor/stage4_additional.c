/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stage4_additional.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/25 13:22:41 by dkovalch          #+#    #+#             */
/*   Updated: 2017/09/24 18:45:46 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "error.h"
#include "string.h"
#include "grammar.h"
#include "string_array.h"
#include "structs_command.h"

/*
**	Line processor - stage 4
**	This function deletes back slashes from the line.
*/

void		lp_s4_replace_bslashes(char **arg)
{
	size_t	ct;
	char	*temp;

	ct = 0;
	temp = NULL;
	while (*arg && (*arg)[ct])
	{
		if (gr_is_bsl(*arg, ct))
		{
			temp = ft_strsub(*arg, 0, ct);
			temp = ft_strjoin_del_first(&temp, &((*arg)[ct + 1]));
			ft_strdel(arg);
			*arg = temp;
			ct += (gr_is_bsl(*arg, ct)) ? 1 : 0;
		}
		else
			ct++;
	}
}

/*
**	Line processor - stage 4
**	This function gets heredoc's stop string and quotatins mark,
**	to apply to all resulting strings later.
*/

static void	get_heredoc_stop(t_heredoc *const heredoc, const char *const str)
{
	if (gr_is_qt(str, 0))
		heredoc->quotmark = ft_strsub(str, 0, 1);
	if (heredoc->herestring)
		heredoc->input = ft_strdup(str);
	else
		heredoc->stop_string = ft_strdup(str);
	str_trim_qts((heredoc->herestring)
		? &(heredoc->input) : &(heredoc->stop_string));
}

/*
**	Line processor - stage 4
**	This function gets info from the string for heredocs.
*/

static void	parse_the_string(t_heredoc *const heredoc, const char *const str)
{
	size_t	ct;

	heredoc->herestring = ft_strnequ(str, "<<<", 3);
	ct = 0;
	while (str[ct] == '<')
		ct++;
	if (str[ct] != '\0')
		get_heredoc_stop(heredoc, &(str[ct]));
}

/*
**	This function checks if we need given argument for
**	any heredoc-related operations.
*/

bool		lp_s4_heredocs(t_command *const token, ssize_t *const arg_no)
{
	bool	no_error;

	no_error = true;
	if (!gr_is_heredoc(token->args[*arg_no], 0)
	&& (token->heredoc == NULL || token->heredoc->stop_string != NULL))
		return (true);
	if (token->heredoc != NULL && token->heredoc->stop_string == NULL)
		get_heredoc_stop(token->heredoc, token->args[*arg_no]);
	else if (ft_strnequ(token->args[*arg_no], "<<<<", 4))
	{
		err_print(ERR_PARSE_AT, token->args[*arg_no]);
		no_error = false;
	}
	else
		parse_the_string(st_hrd_create(token), token->args[*arg_no]);
	str_arr_remove_one((char *const **const)&(token->args), *arg_no);
	(*arg_no)--;
	return (no_error);
}
