/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stage1_cut.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/30 15:57:41 by dkovalch          #+#    #+#             */
/*   Updated: 2017/09/23 11:59:22 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "string.h"
#include "grammar.h"
#include "line_processor_internal.h"

/*
**	Line processor - stage 1
**	This function looks for the next separator.
**
**	Return value:
**		position of the next eparator or null-terminator
*/

static size_t		find_next_separator(const char *line,
							const size_t separator)
{
	size_t		rt;
	const char	*temp;

	if (!line || !line[separator])
		return (separator);
	rt = separator;
	if (gr_is_separator(line, separator))
	{
		while (line[separator] == line[rt])
			rt++;
		return (rt);
	}
	while (!gr_is_separator(line, rt) && line[rt])
	{
		if (gr_is_qt(line, rt))
		{
			temp = str_skip_qts(&(line[rt]));
			rt = (temp) ? (size_t)ABS(temp - line) : ft_strlen(line);
		}
		else
			rt++;
	}
	return (rt);
}

/*
**	Line processor - stage 1
**	This function connects a new node to the list.
*/

static void			connect(t_command **head, t_command **current)
{
	t_command	*temp;

	temp = *head;
	while (temp && temp->next)
		temp = temp->next;
	if (temp)
		temp->next = *current;
	else
		*head = *current;
	*current = NULL;
}

/*
**	Line processor - stage 1
**	This function splits the line by separators and builds a list out of that.
**
**	Return value:
**		true	-	success
**		false	-	fail, with error message already printed
*/

static void			stage1_split_the_line(t_command **head, const char *input)
{
	size_t			separator;
	size_t			next_separator;
	t_command		*current_command;

	*head = NULL;
	separator = 0;
	current_command = NULL;
	while (input && input[separator])
	{
		next_separator = find_next_separator(input, separator);
		current_command = st_cmd_new(
			ft_strsub(input, separator, next_separator - separator));
		str_trim((char**)&(current_command->raw_data));
		connect(head, &current_command);
		separator = next_separator;
	}
}

/*
**	Line processor - stage 1
**	This function calls necessary functions in right order. It is an entry
**	point for the lexer - parser section.
**
**	Return value:
**		true	-	success
**		false	-	fail, with error message already printed
*/

bool				lp_process_line(t_command **head, const char *input)
{
	stage1_split_the_line(head, input);
	lp_s2_rearrange_the_list(head);
	lp_s2_split_into_args(*head);
	return (lp_s3_analyze_the_list(*head));
}
