/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   buildin_env.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/08 10:48:14 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/06 15:57:55 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "ft_printf.h"
#include "error.h"
#include "structs_global.h"
#include "structs_environment.h"
#include "structs_command.h"

/*
**	Printitng the list of variables
**	Refactor me.
*/

bool	cp_buildin_env(const t_command *const cmd)
{
	t_env_var	*variable;

	if (!cmd->args[1])
	{
		variable = g_global->environ_vars;
		while (variable)
		{
			ft_printf("%s=%s\n", variable->name,
				variable->data ? variable->data : "");
			variable = variable->next;
		}
	}
	else
		err_print_long(cmd->name, ERR_MANY_ARGS, cmd->args[1]);
	return (true);
}

/*
**	Deleting one of the variables from the list. Can handle many arguments.
**	Refactor me.
*/

bool	cp_buildin_unsetenv(const t_command *const cmd)
{
	size_t	ct;

	if (!cmd->args[1])
		err_print(cmd->name, ERR_FEW_ARGS);
	else
	{
		ct = 1;
		while (cmd->args[ct])
		{
			if (st_env_find(cmd->args[ct]))
				st_env_remove(cmd->args[ct]);
			else
				err_print("no such variable", cmd->args[ct]);
			ct++;
		}
	}
	return (true);
}

/*
**	Creating/changing one of the variables.
**	Refactor me.
*/

bool	cp_buildin_setenv(const t_command *const cmd)
{
	t_env_var	*the_element;

	if (!cmd->args[1])
		err_print(cmd->name, ERR_FEW_ARGS);
	else if (cmd->args[2] && cmd->args[3])
		err_print_long(cmd->name, ERR_MANY_ARGS, cmd->args[3]);
	else
	{
		the_element = st_env_get(cmd->args[1]);
		ft_strdel(&(the_element->data));
		the_element->data = ft_strdup(cmd->args[2]);
	}
	return (true);
}
