/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stage2_heredoc.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/09 20:01:44 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/06 22:56:14 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <fcntl.h>
#include "error.h"
#include "defines.h"
#include "line_reader.h"
#include "structs_environment.h"
#include "line_processor_internal.h"
#include "command_processor_internal.h"

/*
**	Command processor
**	This function reads user input, until the stop_str is found.
**	If Ctrl+C was read, all buffer clears.
*/

static void	read_the_input(t_heredoc *const heredoc)
{
	char				*buf;
	t_lr_ret_code		ret_code;
	const char *const	ps2 = st_env_get_n_check("PS2", DEFAULT_PS2)->data;

	ft_putstr(ps2);
	while ((ret_code = lr_get_line(&buf, false)) == LR_N_LINE)
	{
		if (ft_strequ(buf, heredoc->stop_string))
			break ;
		heredoc->input = ft_strjoin_ultimate(&(heredoc->input), &buf);
		heredoc->input = ft_strjoin_del_first(&(heredoc->input), "\n");
		ft_putstr(ps2);
	}
	ft_strdel(&buf);
	if (ret_code == LR_CTRL_C)
	{
		ft_strclr(heredoc->input);
		ft_strdel(&(heredoc->input));
	}
}

/*
**	Command processor
**	This function splits user input by \n, and creates a fake command,
**	to use standard parse mechanism of the shell. It add qt marks to every
**	argument, to preserve user's will of parsing logic. After that
**	it concatenates all arguments back, into one string.
**
**	Return value:
**		true	-	success
**		false	-	parsing error, with message already printed
*/

static bool	split_and_parse(t_heredoc *const heredoc)
{
	size_t		ct;
	t_command	*temp;
	bool		result;
	char		*new_arg;

	temp = st_cmd_new(NULL);
	temp->args = ft_strsplit(heredoc->input, '\n');
	ft_strdel(&(heredoc->input));
	ct = 0;
	while (heredoc->quotmark && temp->args && temp->args[ct++])
	{
		new_arg = ft_strjoin(heredoc->quotmark, temp->args[ct - 1]);
		ft_strdel(&(temp->args[ct - 1]));
		temp->args[ct - 1] = ft_strjoin_del_first(&new_arg, heredoc->quotmark);
	}
	result = lp_s3_analyze_the_list(temp);
	ct = 0;
	while (temp->args && temp->args[ct++])
	{
		heredoc->input =
			ft_strjoin_del_first(&(heredoc->input), temp->args[ct - 1]);
		heredoc->input = ft_strjoin_del_first(&(heredoc->input), "\n");
	}
	st_cmd_delete(&temp);
	return (result);
}

/*
**	Command processor
**	This function opens a pipe, writes the input there, and creates redirection
**	structure, to be apllied later to the given command.
*/

static void	do_apply_heredoc(t_command *const command)
{
	int				fds[2];
	t_redirection	*redir;

	if (pipe(fds) != 0)
	{
		err_print(ERR_CRT_PIPE, NULL);
		return ;
	}
	redir = st_red_new(command);
	redir->flag = READ;
	redir->old_fd = fds[STDIN_FILENO];
	redir->new_fd = STDIN_FILENO;
	fcntl(fds[STDIN_FILENO], F_SETFD, FD_CLOEXEC);
	ft_putstr_fd(command->heredoc->input, fds[STDOUT_FILENO]);
	close(fds[STDOUT_FILENO]);
}

/*
**	Command processor
**	This function calls other neccesary functions to apply given heredoc.
*/

void		cp_s2_apply_heredoc(t_command *const command)
{
	if (!command->heredoc->herestring)
		read_the_input(command->heredoc);
	if (split_and_parse(command->heredoc))
		do_apply_heredoc(command);
}
