/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stage4_child.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/06 13:15:33 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/06 13:42:04 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "error.h"
#include <signal.h>
#include "service.h"
#include "structs_command.h"
#include "structs_environment.h"

/*
**	Command processor
**	This function sets right signals disposition for a child process.
**	If we wont reset these signals, we can end up having children, which
**	hang whe they get controlling terminal.
*/

static void	set_child_job_signals(void)
{
	struct sigaction	action;

	ft_bzero((void*)&action, sizeof(action));
	sigemptyset(&(action.sa_mask));
	action.sa_handler = SIG_DFL;
	if (sigaction(SIGTSTP, &action, NULL) != 0
	|| sigaction(SIGTTIN, &action, NULL) != 0
	|| sigaction(SIGURG, &action, NULL) != 0
	|| sigaction(SIGQUIT, &action, NULL) != 0
	|| sigaction(SIGINT, &action, NULL) != 0)
		err_print_fatal(ERR_REG_SIGNAL, NULL);
}

/*
**	Command processor
**	This function is main child function. Merely forks and execves.
*/

void		cp_s4_child_main(const t_command *const cmd)
{
	if (serv_is_dir(cmd->name))
		err_print_fatal(cmd->name, ERR_IS_DIR);
	set_child_job_signals();
	if (err_can_access(cmd->name, X_OK))
	{
		execve(cmd->name, cmd->args, st_env_to_array());
		err_print_fatal("error running the command", "execve call failed");
	}
	exit(EXIT_FAILURE);
}
