/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stage2_redirections.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/03 17:45:03 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/06 22:01:48 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <fcntl.h>
#include "libft.h"
#include "error.h"
#include "service.h"
#include "structs_global.h"
#include "structs_command.h"

/*
**	Command processor
**	This function checks the filename, and opens the file, with
**	appropriate flags, and save the resulting fd.
**
**	Return value:
**		true	-	success
**		false	-	fail, with error message already printed
*/

static bool	open_filename(t_redirection *const node)
{
	if (node->flag == CLOSE)
		return (true);
	if (!node->filename)
		return (false);
	if (serv_is_dir(node->filename))
	{
		err_print(node->filename, ERR_IS_DIR);
		return (false);
	}
	if (!serv_can_write(node->filename, node->flag == READ ? R_OK : W_OK))
	{
		err_print(ERR_PER_DEN, node->filename);
		return (false);
	}
	node->old_fd = open(node->filename,
		((node->flag == READ) ? O_RDONLY : O_WRONLY) |
		((node->flag == WRITE) ? O_CREAT | O_TRUNC : 0) |
		((node->flag == APPEND) ? O_CREAT | O_APPEND : 0),
		S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
	if (node->old_fd < 0)
		err_print("error opening or creating a file", node->filename);
	else
		fcntl(node->old_fd, F_SETFD, FD_CLOEXEC);
	return (node->old_fd >= 0);
}

/*
**	Command processor
**	This function backs up the fd, which we will be redirecting.
*/

static void	dup_this(int *backup, int fd)
{
	*backup = dup(fd);
	fcntl(*backup, F_SETFD, FD_CLOEXEC);
}

/*
**	Command processor
**	This function backs up the fd, which we will be redirecting, and
**	dups old_fd to new_fd, actually creating a redirection.
**
**	Return value:
**		true	-	success
**		false	-	fail in one of system calls
*/

static bool	apply_redirection_parameters(const t_redirection *const node)
{
	bool	result;

	if (g_global->stdin_fd == EMPTY_FILENO && node->new_fd == STDIN_FILENO)
		dup_this(&(g_global->stdin_fd), node->new_fd);
	if (g_global->stdout_fd == EMPTY_FILENO && node->new_fd == STDOUT_FILENO)
		dup_this(&(g_global->stdout_fd), node->new_fd);
	if (g_global->stderr_fd == EMPTY_FILENO && node->new_fd == STDERR_FILENO)
		dup_this(&(g_global->stderr_fd), node->new_fd);
	if (node->flag == CLOSE)
		result = (close(MAX(node->old_fd, node->new_fd)) == 0);
	else
	{
		result = (dup2(node->old_fd, node->new_fd) >= 0);
		if (node->old_fd > MAX(MAX(STDIN_FILENO, STDOUT_FILENO), STDERR_FILENO))
			close(node->old_fd);
	}
	return (result);
}

/*
**	Command processor
**	This function applies all given redirections.
**
**	Return value:
**		true	-	success
**		false	-	fail, with error message already printed
*/

bool		cp_s2_apply_redirections(t_redirection *head)
{
	while (head)
	{
		if ((head->old_fd == EMPTY_FILENO || head->new_fd == EMPTY_FILENO)
			&& !open_filename(head))
			return (false);
		if ((head->old_fd == EMPTY_FILENO && head->new_fd == EMPTY_FILENO)
		|| !apply_redirection_parameters(head))
		{
			err_print("error creating redirection",
				"please double check your parameters");
			return (false);
		}
		head = head->next;
	}
	return (true);
}
