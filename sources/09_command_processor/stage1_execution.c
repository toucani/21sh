/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stage1_execution.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/05 20:59:27 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/08 18:06:27 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <fcntl.h>
#include "error.h"
#include "grammar.h"
#include "command_processor_internal.h"

/*
**	Command processor
**	This function chooses the node, we should run next, depending on
**	separator value.
**
**	value	|	previous	|		node		|
**			|	 result		|					|
**	--------------------------------------------|
**	   &&	|	true		|		next		|
**	   &&	|	false		|	next after next	|
**	   ||	|	true		|	next after next	|
**	   ||	|	false		|		next		|
**	   ;	|				|		next		|
**	   &	|				|		next		|
*/

static void	separator_node(t_command **node, const bool previous_result)
{
	const bool	and = ft_strequ((*node)->raw_data, "&&");

	if (!gr_is_separator((*node)->raw_data, 0))
		return ;
	if (ft_strequ((*node)->raw_data, "||")
	|| ft_strequ((*node)->raw_data, "&&"))
		while (*node && (and ^ previous_result)
		&& !ft_strequ((*node)->raw_data, and ? "||" : "&&"))
			*node = (*node)->next;
	*node = (*node) ? (*node)->next : NULL;
}

/*
**	Command processor
**	This function runs single command.
**
**	Return value:
**		true	-	command exited with code 0
**		false	-	command exited with non-zero code
*/

static bool	command_node(t_command *const node)
{
	bool	result;

	result = true;
	if (node->heredoc)
		cp_s2_apply_heredoc(node);
	if (node->redirections)
		result = cp_s2_apply_redirections(node->redirections);
	if (result)
		result = cp_s2_find_and_execute(node);
	return (result);
}

/*
**	Command processor
**	This function assigns right filedesctiptors to the nodes. It makes
**	piped redirections first, to ensure that we don't interrupt any
**	command redirections afterwards(aka >, >>, <).
*/

static void	assign_fds(t_command *const node, const int fds[2])
{
	t_redirection	*redir;

	redir = (t_redirection*)ft_memalloc(sizeof(t_redirection));
	redir->new_fd = STDIN_FILENO;
	redir->old_fd = fds[STDIN_FILENO];
	redir->flag = READ;
	redir->next = node->redirections;
	node->redirections = redir;
	redir = (t_redirection*)ft_memalloc(sizeof(t_redirection));
	redir->new_fd = STDOUT_FILENO;
	redir->old_fd = fds[STDOUT_FILENO];
	redir->flag = WRITE;
	redir->next = node->piped_commands->redirections;
	node->piped_commands->redirections = redir;
}

/*
**	Command processor
**	This function creates special redirections for piped_commands, and
**	runs them in a sequence.
**
**	Return value:
**		true	-	all commands exited with code 0
**		false	-	the last command exited with non-zero code
*/

static bool	piped_node(t_command *node)
{
	int				fds[2];
	bool			result;

	result = true;
	while (node && node->piped_commands && result)
	{
		if (pipe(fds) != 0)
		{
			err_print(ERR_CRT_PIPE, node->name);
			return (false);
		}
		fcntl(fds[STDIN_FILENO], F_SETFD, FD_CLOEXEC);
		fcntl(fds[STDOUT_FILENO], F_SETFD, FD_CLOEXEC);
		assign_fds(node, fds);
		if (!(result = command_node(node)))
			close(fds[STDOUT_FILENO]);
		node = node->piped_commands;
	}
	return (result && command_node(node));
}

/*
**	Command processor
**	This function executes a command, then asks separator functions,
**	which command to run next.
**	Asking separator node makes possible for us to use && and ||
**	command specifiers(ls -l && echo "Success" || echo "Fail")
**
**	result = true if exit code is 0
*/

void		cp_process_commands(t_command *head)
{
	bool	result;

	while (head)
	{
		if (head->piped_commands)
			result = piped_node(head);
		else
			result = command_node(head);
		head = head->next;
		if (head && head->separator)
			separator_node(&head, result);
	}
}
