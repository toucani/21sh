/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   buildin_echo.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/13 12:37:59 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/06 18:38:45 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "ft_printf.h"
#include "structs_command.h"

/*
**	Command processor - buildins.
**	This functions actually prints arguments using given options.
**
**	In 42sh - options should be replaces with either a bit-flags integer,
**	or bit-field structure(preferable).
*/

static void	run_command(char **args, const int options)
{
	size_t	ct;

	ct = 1;
	while (args[ct] != NULL)
	{
		ft_printf("%s%s", ct > 1 ? " " : "", args[ct]);
		ct++;
	}
	if (options == 1)
		ft_printf("\n");
}

/*
**	Command processor - buildins.
**	This functions is echo entry point.
**
**	Return value:
**		true - command was executed
**		false - command was not executed, due to errors
*/

bool		cp_buildin_echo(const t_command *const cmd)
{
	run_command(cmd->args, 1);
	return (true);
}
