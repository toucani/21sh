/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   buildin_cd.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/13 10:20:33 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/06 15:49:27 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "error.h"
#include <limits.h>
#include "service.h"
#include "ft_printf.h"
#include "structs_command.h"
#include "structs_environment.h"

/*
**	Command processor - buildins.
**	This functions adds HOME path to arguments if they are empty.
**	Refactor me.
*/

static void	cp_buildin_cd_get_home_path(t_command *const cmd)
{
	char		**rt;

	if (cmd->args[1] == NULL)
	{
		rt = (char**)ft_memalloc(sizeof(char*) * 3);
		rt[0] = cmd->args[0];
		rt[1] = ft_strdup(st_env_get_n_check("HOME", getenv("HOME"))->data);
		ft_memdel((void**)&(cmd->args));
		cmd->args = rt;
	}
}

/*
**	Command processor - buildins.
**	This functions checks if we have - in the arguments, and acts accordingly.
**	Refactor me.
*/

static void	cp_buildin_cd_get_back_path(const t_command *const cmd)
{
	t_env_var	*oldpwd;
	t_env_var	*home;

	if (cmd->args[1] != NULL && ft_strequ(cmd->args[1], "-"))
	{
		oldpwd = st_env_get("OLDPWD");
		home = st_env_get_n_check("HOME", getenv("HOME"));
		if (oldpwd->data)
		{
			ft_strdel(&(cmd->args[1]));
			cmd->args[1] = ft_strdup(oldpwd->data);
			ft_printf("%s%s\n", (ft_strstr(oldpwd->data, home->data)) ?
				"~" : "", (ft_strstr(oldpwd->data, home->data)) ?
				&(oldpwd->data[ft_strlen(home->data)]) : oldpwd->data);
		}
		else
			err_print_long(cmd->name, ERR_NO_VAR(OLDPWD), ERR_CHECK_VAR);
	}
}

/*
**	Updating PWD and OLDPWD(or creating them)
**	Refactor me.
*/

static void	cp_buildin_cd_update_env(void)
{
	t_env_var	*pwd;
	t_env_var	*opwd;
	char		*temp;

	opwd = st_env_get("OLDPWD");
	pwd = st_env_get("PWD");
	ft_strdel(&(opwd->data));
	opwd->data = pwd->data;
	if (!(temp = getcwd(NULL, 0)))
		temp = getcwd(ft_strnew(PATH_MAX), PATH_MAX);
	pwd->data = temp;
}

static bool	check_args(const t_command *const cmd)
{
	if (cmd->args[1] && cmd->args[2])
		err_print_long(cmd->name, ERR_MANY_ARGS, cmd->args[2]);
	else if (cmd->args[1] && cmd->args[1][0] == '-' && cmd->args[1][1] != '\0')
		err_print_long(cmd->name, ERR_UNK_OPT, cmd->args[1]);
	else if (cmd->args[1] && !ft_strchr(cmd->args[1], '/')
			&& ft_strlen(cmd->args[1]) > NAME_MAX)
		err_print_long(cmd->name, ERR_FILENAME_TOO_LONG, cmd->args[1]);
	else if (cmd->args[1] && ft_strlen(cmd->args[1]) > PATH_MAX)
		err_print_long(cmd->name, ERR_FILEPATH_TOO_LONG, cmd->args[1]);
	else
		return (true);
	return (false);
}

/*
**	Checking if the command is valid, parsing the path, checking access,
**	and changing the dir.
**	Refactor me.
*/

bool		cp_buildin_cd(const t_command *const cmd)
{
	if (!(check_args(cmd)))
		return (false);
	cp_buildin_cd_get_back_path((t_command*)cmd);
	cp_buildin_cd_get_home_path((t_command*)cmd);
	if (!(err_can_access(cmd->args[1], X_OK)))
		return (false);
	if (chdir(cmd->args[1]) < 0)
		err_print_long(cmd->name, "cannot change path", cmd->args[1]);
	else
		cp_buildin_cd_update_env();
	return (true);
}
