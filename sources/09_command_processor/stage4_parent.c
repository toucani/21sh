/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stage4_parent.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/06 13:32:40 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/06 14:17:58 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "error.h"
#include <signal.h>
#include "service.h"
#include <sys/wait.h>
#include "ft_printf.h"
#include "structs_global.h"
#include "structs_command.h"

/*
**	Command processor
**	This function returns the exit code of the programm taken from status,
**	and prints corresponding message,if the programm was terminated or signaled.
**
**	Return value:
**		true	-	given command exited with code 0, and status 0,
**		false	-	given command exited with non-zero code
*/

static bool		get_status_info(const int status)
{
	char	*message;

	if (WIFEXITED(status))
		return (!WEXITSTATUS(status));
	message = serv_status_get_string(status);
	ft_printf("\n%s\n", message);
	ft_strdel(&message);
	return (false);
}

/*
**	Command processor
**	This function tries to give the controlling terminal to the given process,
**	if it fails, prints an error message.
*/

static void		change_foreground_process(const pid_t pgid)
{
	if (tcsetpgrp(g_global->terminal_fd, pgid) != 0)
		err_print("error setting foreground process",
		"cannot give the terminal to the process");
}

/*
**	Command processor
**	This function sets the command as a foreground process, changes SIGCHLD
**	disposition and waits, returning the exit code after that, if, and only
**	if, this command was not meant to run in background(background command or
**	piped command).
**
**	Return value:
**		true	-	given command exited with code 0, and status 0,
**					or was run background
**		false	-	given command exited with non-zero code
*/

bool			cp_s4_parent_main(const t_command *const cmd)
{
	int					status;
	struct sigaction	old;
	struct sigaction	new;

	if (cmd->background)
		st_chl_push_back(st_chl_new(cmd->pid, cmd->name));
	else if (!cmd->piped_commands)
	{
		status = 0;
		ft_bzero((void*)&new, sizeof(struct sigaction));
		sigemptyset(&(new.sa_mask));
		new.sa_handler = SIG_DFL;
		if (sigaction(SIGCHLD, &new, &old) != 0)
			err_print(ERR_REG_SIGNAL, NULL);
		change_foreground_process(cmd->pgid);
		while (waitpid(-(cmd->pgid), &status, 0) != -1)
			;
		change_foreground_process(g_global->sh_pgid);
		tcsetattr(g_global->terminal_fd, TCSADRAIN, &(g_global->terminal_mine));
		if (sigaction(SIGCHLD, &old, NULL) != 0)
			err_print(ERR_REG_SIGNAL, NULL);
		return (get_status_info(status));
	}
	return (true);
}
