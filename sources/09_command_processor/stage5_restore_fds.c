/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stage5_restore_fds.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/08 11:12:21 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/06 14:16:48 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "error.h"
#include <unistd.h>
#include "structs_global.h"

/*
**	Command processor
**	Unified function to move one filedescriptor to another, with error
**	message printed in case of error.
**	man, wish we had lambdas in c...
*/

static void	close_and_clear(int *const old_fd, const int new_fd)
{
	if (*old_fd >= 0 && new_fd >= 0)
	{
		if (new_fd != dup2(*old_fd, new_fd) || close(*old_fd) < 0)
			err_print_fatal("error reassigning filedescriptors", NULL);
		else
			*old_fd = EMPTY_FILENO;
	}
}

/*
**	Command processor
**	Restoring filedescriptors. Moving STDIN, STDOUT, STDERR.
*/

void		cp_s5_restore_fds(void)
{
	close_and_clear(&(g_global->stdin_fd), STDIN_FILENO);
	close_and_clear(&(g_global->stdout_fd), STDOUT_FILENO);
	close_and_clear(&(g_global->stderr_fd), STDERR_FILENO);
}
