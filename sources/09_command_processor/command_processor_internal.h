/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   command_processor_internal.h                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/23 12:11:44 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/06 13:55:52 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef COMMAND_PROCESSOR_INTERNAL_H
# define COMMAND_PROCESSOR_INTERNAL_H

/*
**	It is internal file which provides all internal functions for
**	command processor.
**
**	Should be used:
**	'command processor' section of the program only.
**
**	Function naming:
**	lp_...
*/

# include "libft.h"
# include "structs_command.h"
# include "structs_redirection.h"

void	cp_s2_apply_heredoc(t_command *const cmd);
bool	cp_s2_apply_redirections(t_redirection *head);
bool	cp_s2_find_and_execute(t_command *const cmd);
bool	cp_s3_run_external(t_command *const cmd);
void	cp_s4_child_main(const t_command *const cmd) __attribute__((noreturn));
bool	cp_s4_parent_main(const t_command *const cmd);
void	cp_s5_restore_fds(void);

/*
**	Build in fucntion prototypes must correspond to
**	t_command->buildin prototype.
*/

bool	cp_buildin_cd(const t_command *const cmd);
bool	cp_buildin_env(const t_command *const cmd);
bool	cp_buildin_echo(const t_command *const cmd);
bool	cp_buildin_setenv(const t_command *const cmd);
bool	cp_buildin_history(const t_command *const cmd);
bool	cp_buildin_unsetenv(const t_command *const cmd);

#endif
