/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   buildin_history.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/11 11:05:21 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/04 15:36:21 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "error.h"
#include "defines.h"
#include "ft_printf.h"
#include "structs_global.h"
#include "structs_command.h"

/*
**	Build-in command help printer.
**	Do we need this function for other buildins?
**	Refactor me.
*/

static void	print_cp_buildin_help(const char *const *const lines)
{
	size_t	ct;
	if (lines && *lines)
	{
		ct = 0;
		ft_printf("\nBuild-in command: %r%s%r\n\n", "bold",
			lines[ct++], "reset");
		while (lines[ct])
			ft_putendl(lines[ct++]);
		ft_putendl("");
	}
}

/*
**	Command processor - buildins.
**	This functions checks the argument we have and their amount.
**
**	Return value:
**		true - arguments are ok
**		false - arguments are not ok, with error already printed
**	Refactor me.
*/

static bool	check_command(const t_command *const cmd)
{
	if (cmd->args[1] && !ft_strequ(cmd->args[1], "-c")
	&& !ft_strequ(cmd->args[1], "-h"))
		err_print_long(cmd->name, ERR_UNK_OPT, cmd->args[1]);
	else if (cmd->args[1] && cmd->args[2])
		err_print_long(cmd->name, ERR_MANY_ARGS, cmd->args[2]);
	else
		return (true);
	return (false);
}

/*
**	Command processor - buildins.
**	This functions prints all history that we have.
**	Refactor me.
*/

static void	print_history(void)
{
	size_t			ct;
	t_history_list	*elem;

	ct = 0;
	elem = g_global->history;
	while (elem && elem->prev)
		elem = elem->prev;
	while (elem)
	{
		ft_printf("%4u: %s\n", ++ct, elem->line);
		elem = elem->next;
	}
}

/*
**	Command processor - buildins.
**	This functions generates help,
**	to be printed with print_cp_buildin_help function.
**	Refactor me.
*/

static void	print_help(void)
{
	const char *help[] =
	{
		"history",
		"usage:\thistory [option]",
		"",
		"Manages history of typed-in commands.",
		"Current history limit is "STR(HISTORY_LIMIT)".",
		"",
		"Option\tMeaning",
		"  -c\tclears entire history",
		"  -h\tprints this message",
		"",
		"Only 1 option can be specified at a time.",
		"The limit cannot be changed at a run-time,",
		"it is a compile-time constant. If you wish",
		"to change it, please take a look at a definition",
		"header file ("DEFAULT_SHELL_NAME"_defines.h).",
		"History is not stored in any file, it is saved",
		"in RAM. You can use [history > file.txt]",
		"to save your history.",
		NULL
	};
	print_cp_buildin_help(help);
}

/*
**	Command processor - buildins.
**	This functions is history entry point.
**
**	Return value:
**		true - command was executed
**		false - command was not executed, due to errors in arguments
*/

bool		cp_buildin_history(const t_command *const command)
{
	if (!check_command(command))
		return false;
	if (!command->args[1])
		print_history();
	else if (command->args[1] && ft_strequ(command->args[1], "-c"))
	{
		st_hst_clear(g_global->history);
		ft_putendl("History has been cleaned.");
	}
	else if (command->args[1] && ft_strequ(command->args[1], "-h"))
		print_help();
	return (true);
}
