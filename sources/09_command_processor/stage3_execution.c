/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stage3_execution.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/06 13:15:33 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/06 14:51:35 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "command_processor_internal.h"

/*
**	Command processor
**	This function decides which prosess group does current command belong to,
**	and sets right pgid into the command.
**
**	Return value:
**		pgid of the group, given command must be placed into.
*/

static pid_t	set_right_pgid(t_command *const cmd, const int pid)
{
	cmd->pid = pid;
	if (cmd->pgid == 0)
		cmd->pgid = cmd->pid;
	if (cmd->piped_commands)
		cmd->piped_commands->pgid = cmd->pgid;
	return (cmd->pgid);
}

/*
**	Command processor
**	This function forks and moves child process into its process group.
**
**	Return value:
**		true	-	given command exited with code 0, status 0
**					or was run background
**		false	-	given command exited with non-zero code
*/

bool			cp_s3_run_external(t_command *const cmd)
{
	const pid_t			pid = fork();
	const pid_t			pgid = set_right_pgid(cmd, pid);

	setpgid(pid, pgid);
	if (pid == 0)
		cp_s4_child_main(cmd);
	cp_s5_restore_fds();
	return (cp_s4_parent_main(cmd));
}
