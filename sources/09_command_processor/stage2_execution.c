/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stage2_execution.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/06 12:56:18 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/06 16:01:31 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "error.h"
#include <dirent.h>
#include "service.h"
#include <sys/wait.h>
#include "structs_global.h"
#include "command_processor_internal.h"

/*
**	Command processor
**	This function checks if the command name is one of build-ins, and sets
**	the function pointer to the corresponding function.
**
**	Return value:
**		true	-	command was found
**		false	-	command was not found
*/

static bool	set_buildin(t_command *const cmd)
{
	if (ft_strequ(cmd->name, "exit"))
		exit(EXIT_SUCCESS);
	else if (ft_strequ(cmd->name, "cd"))
		cmd->buildin = cp_buildin_cd;
	else if (ft_strequ(cmd->name, "env"))
		cmd->buildin = cp_buildin_env;
	else if (ft_strequ(cmd->name, "echo"))
		cmd->buildin = cp_buildin_echo;
	else if (ft_strequ(cmd->name, "setenv"))
		cmd->buildin = cp_buildin_setenv;
	else if (ft_strequ(cmd->name, "unsetenv"))
		cmd->buildin = cp_buildin_unsetenv;
	else if (ft_strequ(cmd->name, "history"))
		cmd->buildin = cp_buildin_history;
	else
		return (false);
	return (true);
}

/*
**	Command processor
**	This function looks for a command in the folders, specified by PATH
**	environment variable. If the command is found, it's name (cmd->name)
**	is changed to full path of the command.
**
**	Known bug:
**	errno can be set to 2(no such dir) in this function, while
**	calling serv_can_access for some of the paths in g_global->paths
**
**	Return value:
**		true	-	command was found
**		false	-	command was not found
*/

static bool	set_external(t_command *const cmd)
{
	size_t			ct;
	char			**paths;
	DIR				*directory;
	struct dirent	*element;

	ct = 0;
	paths = ft_strsplit(st_env_get("PATH")->data, ':');
	while (paths && paths[ct])
	{
		if (!ft_strchr(cmd->name, '/') && serv_can_access(paths[ct], R_OK)
		&& (directory = opendir(paths[ct])))
		{
			while (!ft_strchr(cmd->name, '/') && (element = readdir(directory)))
				if (ft_strequ(cmd->name, element->d_name))
				{
					ft_strdel((char**)&(cmd->name));
					cmd->name = ft_strjoin_f(paths[ct], "/",
						element->d_name, "");
				}
			closedir(directory);
		}
		ft_strdel(&(paths[ct++]));
	}
	ft_memdel((void**)&paths);
	return (ft_strchr(cmd->name, '/'));
}

/*
**	Command processor
**	This function update the _ variable with the full path
**	to the executed command.
*/

static void	update_variable(const t_command *const cmd)
{
	t_env_var	*variable;

	variable = st_env_get("_");
	ft_strdel(&(variable->data));
	variable->data = ft_strdup(cmd->name);
}

/*
**	Command processor
**	This function executes given command and returns the result of execution.
**	When a buildin is in a pipe with other processes, and it's first command,
**	we wait for other processes from the same group to finish, before returning.
**
**	Return value:
**		true	-	given command exited with code 0
**		false	-	given command exited with non-zero code
*/

bool		cp_s2_find_and_execute(t_command *const cmd)
{
	bool	status;

	status = false;
	if (!cmd)
		return (false);
	if (set_buildin(cmd))
	{
		status = cmd->buildin(cmd);
		cp_s5_restore_fds();
		if (!cmd->piped_commands && cmd->pgid > 0)
			while (waitpid(-(cmd->pgid), NULL, 0) != -1)
				;
	}
	else if (set_external(cmd))
		status = cp_s3_run_external(cmd);
	else
		err_print(ERR_CMD_NOT_FOUND, cmd->name);
	if (status)
		update_variable(cmd);
	return (status);
}
