/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/20 11:37:37 by dkovalch          #+#    #+#             */
/*   Updated: 2017/09/24 20:25:42 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "main.h"
#include "help.h"
#include "libft.h"
#include "error.h"
#include "loop_runner.h"
#include "structs_global.h"
#include "cursor_movements.h"

t_global_info	*g_global = NULL;

/*
**	Main - main
**	This function clears the history and restores terminal flags as they were
**	before runnning the program.
*/

static void	onexit(void)
{
	st_hst_clear(g_global->history);
	tcsetattr(STDIN_FILENO, TCSANOW, &(g_global->terminal_origin));
}

/*
**	Main - main
**	This function is MAAAIIIIIIIIIIIIIIIIIIIIIIN!!!
*/

int			main(const int ac, const char **av, const char **env)
{
	if ((av[1] && ft_strequ(av[1], "--help")) ||
		(av[1] && av[2] && ft_strequ(av[2], "--help")))
		h_print_help();
	g_global = (t_global_info*)ft_memalloc(sizeof(t_global_info));
	m_init_global(av[0], env);
	if (ac > 3)
		err_print_fatal(ERR_MANY_ARGS, av[1]);
	atexit(onexit);
	m_init_variables(av[1], av[1] ? av[2] : NULL);
	m_init_terminal();
	cm_init_termcap();
	m_init_signals();
	lr_loop_run();
	exit(EXIT_SUCCESS);
}
