/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   initialization.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/18 20:59:36 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/06 09:08:36 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "error.h"
#include <fcntl.h>
#include <termcap.h>
#include "defines.h"
#include <sys/ioctl.h>
#include "structs_global.h"

/*
**	Main - variables
**	This function add internal variables to the global list of variables.
**	PROMPT, PROMPT_CLR
*/

static void	m_add_internal_vars(const char *const prompt,
									const char *const color)
{
	st_env_push_back(st_env_new_d(DEFAULT_PROMPT_VAR,
					prompt ? prompt : DEFAULT_PROMPT));
	st_env_push_back(st_env_new_d(DEFAULT_PROMPT_CLR_VAR,
					color ? color : DEFAULT_PROMPT_CLR));
}

/*
**	Main - variables
**	This function updates, or creates, SHLVL variable.
*/

static void	update_shlvl_var(void)
{
	size_t		level;
	t_env_var	*variable;

	variable = st_env_get("SHLVL");
	level = ((variable->data) ? ft_atoi(variable->data) : 0) + 1;
	ft_strdel(&(variable->data));
	variable->data = ft_itoa(level);
}

/*
**	Main - variables
**	This function initializes necessary variables. It must be called once,
**	at the start of the program.
*/

void		m_init_variables(const char *const av1,
								const char *const av2)
{
	m_add_internal_vars(av1, av2);
	update_shlvl_var();
}

/*
**	Main - initializing
**	This function initalizes the g_global structure
*/

void		m_init_global(const char *const av0, const char **const env)
{
	size_t ct;

	g_global->stdin_fd = EMPTY_FILENO;
	g_global->stdout_fd = EMPTY_FILENO;
	g_global->stderr_fd = EMPTY_FILENO;
	*((const char**)&(g_global->program_name)) = (ft_strrchr(av0, '/')) + 1;
	*((pid_t*)(&(g_global->sh_pid))) = getpid();
	*((pid_t*)(&(g_global->sh_pgid))) = getpgid(g_global->sh_pid);
	ct = 0;
	while (env && env[ct])
		st_env_push_back(st_env_new(env[ct++]));
}

/*
**	Main - initializing
**	This function initalizes the terminal and saves previous terminal flags.
*/

void		m_init_terminal(void)
{
	int				fd;

	if ((fd = open("/dev/tty", O_RDWR | O_NONBLOCK)))
	{
		tcgetattr(fd, &(g_global->terminal_origin));
		tcgetattr(fd, &(g_global->terminal_mine));
		g_global->terminal_mine.c_cflag |= CLOCAL | CREAD | PARENB | CS8;
		g_global->terminal_mine.c_iflag |= INPCK | IGNPAR | IXOFF | IXON;
		g_global->terminal_mine.c_iflag &= ~(ISTRIP | BRKINT);
		g_global->terminal_mine.c_lflag &=
			~(ECHO | ECHOE | ECHONL | ICANON | IEXTEN);
		g_global->terminal_mine.c_lflag |= ISIG | TOSTOP;
		g_global->terminal_mine.c_cc[VMIN] = 1;
		g_global->terminal_mine.c_cc[VTIME] = 0;
		tcsetattr(fd, TCSANOW, &(g_global->terminal_mine));
		*((int*)(&g_global->terminal_fd)) = fd;
		ioctl(g_global->terminal_fd, TIOCGWINSZ, &(g_global->terminal_size));
		fcntl(fd, F_SETFD, FD_CLOEXEC);
	}
}
