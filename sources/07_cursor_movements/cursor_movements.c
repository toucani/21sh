/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cursor_movements.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/24 19:04:39 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/03 13:24:37 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include <termcap.h>
#include "cursor_movements.h"
#include "structs_global.h"

/*
**	Cursor movements
**	This function moves cursor up. It scrools and updates internal cursor
**	structure automatically.
*/

void		cm_cursor_move_up(unsigned amount)
{
	if (!g_cur_pos->is_valid)
		cm_global_init();
	if ((int)amount < g_cur_pos->line)
		g_cur_pos->line -= amount;
	else
	{
		g_cur_pos->v_scroll -= amount - g_cur_pos->line;
		g_cur_pos->line = 1;
	}
	cm_cursor_apply();
}

/*
**	Cursor movements
**	This function moves cursor down. It scrools and updates internal cursor
**	structure automatically.
*/

void		cm_cursor_move_down(unsigned amount)
{
	if (!g_cur_pos->is_valid)
		cm_global_init();
	if ((int)amount <= g_global->terminal_size.ws_row - g_cur_pos->line)
		g_cur_pos->line = g_global->terminal_size.ws_row;
	else
	{
		g_cur_pos->v_scroll +=
			amount - (g_global->terminal_size.ws_row - g_cur_pos->line);
		g_cur_pos->line = g_global->terminal_size.ws_row;
	}
	cm_cursor_apply();
}

/*
**	Cursor movements
**	This function moves cursor left. It scrools and updates internal cursor
**	structure automatically.
**
**	Not sure if we need the case ((int)amount == g_cur_pos->col)
*/

void		cm_cursor_move_left(unsigned amount)
{
	int	v_offset;
	int	h_offset;

	if (!g_cur_pos->is_valid)
		cm_global_init();
	if ((int)amount < g_cur_pos->col)
		g_cur_pos->col = MAX(1, g_cur_pos->col - amount);
	else if ((int)amount == g_cur_pos->col)
		cm_cursor_add_offset(-1,
			g_global->terminal_size.ws_col - g_cur_pos->col);
	else
	{
		cm_cursor_count_offset(-amount, &v_offset, &h_offset);
		cm_cursor_add_offset(v_offset, h_offset);
	}
	cm_cursor_apply();
}

/*
**	Cursor movements
**	This function moves cursor right. It scrools and updates internal cursor
**	structure automatically.
*/

void		cm_cursor_move_right(unsigned amount)
{
	int	v_offset;
	int	h_offset;

	if (!g_cur_pos->is_valid)
		cm_global_init();
	if ((int)amount <= g_global->terminal_size.ws_col - g_cur_pos->col)
		g_cur_pos->col = MIN(g_global->terminal_size.ws_col,
			g_cur_pos->col + amount);
	else
	{
		cm_cursor_count_offset(amount, &v_offset, &h_offset);
		cm_cursor_add_offset(v_offset, h_offset);
	}
	cm_cursor_apply();
}
