/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cursor_misc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/24 20:12:46 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/05 16:16:41 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include <termcap.h>
#include "structs_global.h"
#include "cursor_movements.h"

/*
**	Cursor movements
**	This function rings a bell:)
*/

void	cm_bell(void)
{
	tputs(tgetstr("bl", NULL), 1, cm_putchar);
}

/*
**	Cursor movements
**	This function is just a simple printer with internal
**	cursor structure auto-update. It add offset line by line, and adds vertical
**	offset if founds a '\n'.
*/

void	cm_print(const char *const str)
{
	const char	*temp;
	int			v_off;
	int			h_off;

	ft_putstr(str);
	temp = str;
	while (temp)
	{
		cm_cursor_count_offset(ft_strlen_dl(temp, '\n'), &v_off, &h_off);
		cm_cursor_add_offset(v_off, h_off);
		if ((temp = ft_strchr(temp, '\n')))
		{
			temp++;
			cm_cursor_add_offset(1, -(g_cur_pos->col - 1));
		}
	}
}

/*
**	Cursor movements
**	This function rmoves cursor to a new line.
*/

void	cm_new_line(void)
{
	if (g_cur_pos->line < g_global->terminal_size.ws_row)
		g_cur_pos->line++;
	else
		g_cur_pos->v_scroll++;
	g_cur_pos->col = 1;
	cm_cursor_apply();
	cm_line_clear(CLEAR_FROM_HERE);
}

/*
**	Cursor motion
**	Moves cursor home.
*/

void	cm_cursor_home(void)
{
	g_cur_pos->col = 1;
	g_cur_pos->line = 1;
	tputs(tgetstr("ho", NULL), g_cur_pos->line, cm_putchar);
}

/*
**	Cursor movements
**	This function is internal putchar, used by termcaps.
*/

int		cm_putchar(int ch)
{
	return ((int)ft_putchar(ch));
}
