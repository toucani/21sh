/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cursor_logic.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/01 19:16:22 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/05 16:15:25 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include <termcap.h>
#include "structs_global.h"
#include "cursor_movements.h"

/*
**	Cursor movements
**	This function recounts horizontal offset into vertical and horizontal.
**	Accepts amount of symbols we need to move with a sign.
**	Amount < 0 means we want to go left.
*/

void		cm_cursor_count_offset(const int h_am, int *v_offset, int *h_offset)
{
	int	current_line_offset;

	if (!g_cur_pos->is_valid)
		cm_global_init();
	*v_offset = h_am / g_global->terminal_size.ws_col;
	*h_offset = h_am % g_global->terminal_size.ws_col;
	current_line_offset = g_cur_pos->col + *h_offset;
	if (current_line_offset < 0
	|| current_line_offset >= g_global->terminal_size.ws_col)
	{
		*v_offset += current_line_offset < 0 ? -1 : 1;
		*h_offset += current_line_offset < 0 ? g_global->terminal_size.ws_col
			: -g_global->terminal_size.ws_col;
	}
}

/*
**	Cursor movements
**	This function add given offset, taking into account edges of the screen.
*/

void		cm_cursor_add_offset(const int v_offset, const int h_offset)
{
	if (!g_cur_pos->is_valid)
		cm_global_init();
	if ((g_cur_pos->col += h_offset) <= 0)
	{
		g_cur_pos->line +=
			(g_cur_pos->col / g_global->terminal_size.ws_col) - 1;
		g_cur_pos->col = g_global->terminal_size.ws_col -
			(-(g_cur_pos->col) % g_global->terminal_size.ws_col);
	}
	if (g_cur_pos->col > g_global->terminal_size.ws_col)
	{
		g_cur_pos->line += (g_cur_pos->col / g_global->terminal_size.ws_col);
		g_cur_pos->col = g_cur_pos->col % g_global->terminal_size.ws_col;
	}
	if ((g_cur_pos->line += v_offset) < 0)
	{
		g_cur_pos->v_scroll += g_cur_pos->line;
		g_cur_pos->line = 1;
	}
	if (g_cur_pos->line > g_global->terminal_size.ws_row)
	{
		g_cur_pos->v_scroll += g_global->terminal_size.ws_row - g_cur_pos->line;
		g_cur_pos->line = g_global->terminal_size.ws_row;
	}
}

/*
**	Cursor movements
**	This function applies all changes made to global cursor position.
**	We set position, and scrool after that, because so ternimals dont actually
**	scrool, but just move the caret when it's not on the edge line.
*/

void		cm_cursor_apply(void)
{
	cm_cursor_set_position(g_cur_pos->col, g_cur_pos->line);
	while (g_cur_pos->v_scroll < 0)
	{
		tputs(tgetstr("sr", NULL), 1, cm_putchar);
		g_cur_pos->v_scroll++;
	}
	while (g_cur_pos->v_scroll > 0)
	{
		tputs(tgetstr("sf", NULL), 1, cm_putchar);
		g_cur_pos->v_scroll--;
	}
	cm_cursor_set_position(g_cur_pos->col, g_cur_pos->line);
}

/*
**	Cursor movement
**	This function returns current position of the cursor.
**
**	Be aware:
**	1)	It is a very slow function, it can crash your program if called faster
**		than ~1 time per second.
**	2)	If you are using the terminal, which doent support reporting cursor
**		position in ASCII format, This function can be blocked in
**		read forever.
*/

t_position	cm_cursor_get_position(void)
{
	t_position	pos;
	char		buf[21];
	char		*temp;

	pos.col = -1;
	pos.line = -1;
	pos.v_scroll = 0;
	pos.is_valid = false;
	if (isatty(STDOUT_FILENO))
	{
		ft_bzero((void*)buf, 21);
		ft_putstr_fd(ESC"[6n", STDOUT_FILENO);
		if (!read(STDIN_FILENO, buf, 20))
			return (pos);
		if ((temp = ft_strchr(buf, '[')))
			pos.line = ft_atoi(temp + 1);
		if ((temp = ft_strchr(buf, ';')))
			pos.col = ft_atoi(temp + 1);
		pos.is_valid = true;
	}
	return (pos);
}

/*
**	Cursor motion
**	Puts cursor at exact, 1 based position.
*/

void		cm_cursor_set_position(const unsigned col, const unsigned line)
{
	g_cur_pos->col = col;
	g_cur_pos->line = line;
	g_cur_pos->is_valid = true;
	tputs(tgoto(tgetstr("cm", NULL), col - 1, line - 1),
		g_global->terminal_size.ws_row, cm_putchar);
}
