/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cursor_clear.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/24 19:35:06 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/01 20:22:54 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include <termcap.h>
#include "ft_printf.h"
#include "structs_global.h"
#include "cursor_movements.h"

/*
**	Cursor motion
**	Clears the line, depending on given mode.
*/

void	cm_line_clear(const t_clear_type type)
{
	ft_printf(ESC"[%uK", type);
}

/*
**	Cursor motion
**	Clears the screen, depending on given mode.
*/

void	cm_screen_clear(const t_clear_type type)
{
	ft_printf(ESC"[%uJ", type);
}

/*
**	Cursor motion
**	Clears the screen, ans sets cursor home.
*/

void	cm_screen_erase(void)
{
	tputs(tgetstr("cl", NULL), g_global->terminal_size.ws_row, cm_putchar);
	cm_cursor_home();
}
