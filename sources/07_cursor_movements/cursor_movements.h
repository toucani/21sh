/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cursor_movements.h                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/12 16:12:58 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/03 13:18:09 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CURSOR_MOVEMENTS_H
# define CURSOR_MOVEMENTS_H

/*
**	This file provides cursor movement functions prototypes.
**
**	Should be used:
**	'line reader' sections of the program, and everywhere manual
**	terminal cursor positionning is needed.
**
**	Functions naming:
**	cm_...
*/

# define ESC "\x1B"
# include <stdbool.h>

/*
**	Position struct. Is used to store cursor position.
**
**	line			|	Cursor line position. Must be int!
**	col				|	Cursor column position. Must be int!
**	v_scrool		|	Is used if we counted the position, but not yet moved
**					|	the cursor, and we have to have some scrolling
**					|	Is used in key_actions mainly. Must be int!
**	is_valid		|	If false - window was resized, and we need
**					|	to call update(), before any cursor movements
*/

typedef struct		s_position
{
	int				line;
	int				col;
	int				v_scroll;
	bool			is_valid;
}					t_position;

extern t_position	*g_cur_pos;

/*
**	These values are used with clear functinos, to specify clearing scope.
*/

typedef enum		e_clear_type
{
	CLEAR_FROM_HERE = 0,
	CLEAR_TO_HERE,
	CLEAR_ALL
}					t_clear_type;

/*
**	cm_init_termcap	should be used only once when initializing the app!
**	cm_global_init	should be used to init the structure or update cursor
**					structure. Shouldn't be called more that 1 time per sec.
*/

void				cm_init_termcap(void);
void				cm_global_init(void);
void				cm_global_clear(void);

void				cm_cursor_move_up(unsigned amount);
void				cm_cursor_move_down(unsigned amount);
void				cm_cursor_move_left(unsigned amount);
void				cm_cursor_move_right(unsigned amount);

void				cm_line_clear(const t_clear_type type);
void				cm_screen_clear(const t_clear_type type);
void				cm_screen_erase(void);

void				cm_bell(void);
void				cm_print(const char *const str);
void				cm_new_line(void);
void				cm_cursor_home(void);

void				cm_cursor_count_offset(const int h_am,
						int *v_offset, int *h_offset);
void				cm_cursor_add_offset(const int v_offset,
						const int h_offset);
void				cm_cursor_apply(void);
t_position			cm_cursor_get_position(void);
void				cm_cursor_set_position(const unsigned col,
						const unsigned line);

/*
**	Functions below are internal.
**	Should be used only in 'cursor movements' section.
*/

int					cm_putchar(int ch);

#endif
