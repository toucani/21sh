/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cursor_init.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/24 19:04:39 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/03 13:27:20 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "error.h"
#include <termcap.h>
#include "structs_global.h"
#include "cursor_movements.h"

t_position	*g_cur_pos = NULL;

/*
**	Cursor movement
**	This function initializes and checks the terminal. Must be called once only!
**	Now it is being called from main(). Can generate fatal error.
*/

void		cm_init_termcap(void)
{
	if (tgetent(NULL, NULL) < 1)
		err_print_fatal(ERR_INIT_TERMINAL, "tgetent failure");
	if (tgetstr("cm", NULL) == NULL || tgetstr("ho", NULL) == NULL
	|| tgetstr("sf", NULL) == NULL || tgetstr("sr", NULL) == NULL
	|| tgetstr("cl", NULL) == NULL || tgetstr("bl", NULL) == NULL
	|| tgetflag("xb") != 0 || tgetflag("hz") != 0)
		err_print_fatal(ERR_INIT_TERMINAL,
			"missing or unsupported capability detected");
	PC = tgetstr("pc", NULL) ? tgetstr("pc", NULL)[0] : '\0';
	ospeed = cfgetospeed(&(g_global->terminal_mine));
}

/*
**	Cursor movement
**	This function initializes global cursor position structure.
**	It must be called before using any of cursor movement functions,
**	and it must be updated every time cursor position changes outside
**	of these functions.
*/

void		cm_global_init(void)
{
	if (!g_cur_pos)
		g_cur_pos = ft_memalloc(sizeof(t_position));
	*g_cur_pos = cm_cursor_get_position();
}

/*
**	Cursor movement
**	This function deletes global cursor position structure.
**	It must be called after you're finished with using these functions.
*/

void		cm_global_clear(void)
{
	if (g_cur_pos)
		ft_memdel((void**)&g_cur_pos);
}
