/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   redirection.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/20 13:18:44 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/04 17:18:53 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "structs_command.h"

/*
**	Structures - redirections.
**	This function creates new redirection struct, and pushes
**	it back into given command.
**
**	Return value:
**		pointer to a newly created structure
*/

t_redirection	*st_red_new(t_command *const token)
{
	t_redirection	*rt;
	t_redirection	*last;

	rt = (t_redirection*)ft_memalloc(sizeof(t_redirection));
	rt->old_fd = EMPTY_FILENO;
	rt->new_fd = EMPTY_FILENO;
	last = st_red_get_last(token);
	if (last)
		last->next = rt;
	else
		token->redirections = rt;
	return (rt);
}

/*
**	Structures - redirections.
**	This function looks for the last redirection structure in given command.
**
**	Return value:
**		pointer to the last redirection structure
**		NULL if there are no redirections in this command
*/

t_redirection	*st_red_get_last(const t_command *const token)
{
	t_redirection	*rt;

	rt = token->redirections;
	while (rt && rt->next)
		rt = rt->next;
	return (rt);
}

/*
**	Structures - redirections.
**	This function deletes all redirections from given command.
*/

void			st_red_delete_all(t_command *const *node)
{
	t_redirection	*head;
	t_redirection	*temp;

	head = (*node)->redirections;
	while (head)
	{
		temp = head->next;
		ft_strdel(&(head->filename));
		ft_memdel((void**)&head);
		head = temp;
	}
	(*node)->redirections = NULL;
}
