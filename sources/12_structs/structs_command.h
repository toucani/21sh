/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   structs_command.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/23 21:44:20 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/06 14:05:41 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef STRUCTS_COMMAND_H
# define STRUCTS_COMMAND_H

/*
**	This file provides command struct, and functions helpers.
**
**	Should be used:
**	everywhere we're working with commands.
**
**	Function naming:
**	st_cmd_...
*/

# include <unistd.h>
# include <stdbool.h>
# include "structs_heredoc.h"
# include "structs_redirection.h"

# ifndef EMPTY_FILENO
#  define EMPTY_FILENO	-1
# endif

/*
**			Command structure:
**
**	raw_data		|	Part of the input line, asossiated with this command
**	name			|	Full path to the command, is used while executing
**	separator		|	Tells us that this is not a command, but a separator
**	background		|	Tell us to run this command in background
**	args			|	All command arguments, ready to use
**	buildin			|	A pointer to a build-in command
**	redirections	|	The list of redirections
**	heredoc			|	Heredoc/herestring info
**	piped_commands	|	The list of piped commands
**	pid				|	PID of the command
**	pgid			|	PGID of the command
*/

typedef struct			s_command
{
	const char *const	raw_data;
	const char			*name;
	bool				separator;
	bool				background;
	char				**args;
	bool				(*buildin)(const struct s_command *const cmd);
	t_redirection		*redirections;
	t_heredoc			*heredoc;
	struct s_command	*piped_commands;
	pid_t				pid;
	pid_t				pgid;
	struct s_command	*next;
}						t_command;

t_command				*st_cmd_new(const char *raw_data);
void					st_cmd_delete(t_command *const *node);
void					st_cmd_delete_list(t_command *const *head);

#endif
