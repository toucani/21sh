/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   environment2.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/10 13:41:27 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/04 16:54:49 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "defines.h"
#include "service.h"
#include "structs_global.h"
#include "structs_environment.h"

/*
**	Structures - environment.
**	This function deletes element with given name, and relinks the list.
*/

void		st_env_remove(const char *name)
{
	t_env_var	*prev;
	t_env_var	*temp;
	t_env_var	*delete;

	prev = 0;
	temp = g_global->environ_vars;
	while (temp)
	{
		delete = 0;
		if (ft_strequ(temp->name, name))
		{
			if (prev)
				prev->next = temp->next;
			else
				g_global->environ_vars = temp->next;
			delete = temp;
			temp = temp->next;
			st_env_delete(&delete);
		}
		else
		{
			prev = temp;
			temp = temp->next;
		}
	}
}

/*
**	Structures - environment.
**	This function looks for a variable with given name. Creates one,
**	if it cannot be found.
**
**	Return value:
**		pointer to the element with given name.
*/

t_env_var	*st_env_get(const char *const name)
{
	t_env_var	*rt;

	if ((rt = st_env_find(name)) == NULL)
	{
		rt = st_env_new_d(name, NULL);
		st_env_push_back(rt);
	}
	return (rt);
}

/*
**	Structures - environment.
**	This function looks for a variable with given name. Creates one,
**	if it cannot be found. If the variable's value is empty, assigns
**	given one.
**
**	Return value:
**		pointer to the element with given name.
*/

t_env_var	*st_env_get_n_check(const char *const name,
									const char *const default_value)
{
	t_env_var	*rt;

	rt = st_env_get(name);
	if (!(rt->data))
		rt->data = ft_strdup(default_value);
	return (rt);
}

/*
**	Structures - environment.
**	This function generates and array of null-terminated strings, from
**	environment variables. Is used to send all environment variables to
**	the child process.
**
**	Return value:
**		pointer to a newly created array.
*/

char		**st_env_to_array(void)
{
	size_t		ct;
	char		**rt;
	t_env_var	*temp;

	ct = 0;
	temp = g_global->environ_vars;
	while (temp)
	{
		if (!serv_env_is_internal(temp))
			ct++;
		temp = temp->next;
	}
	rt = (char**)ft_memalloc(sizeof(char*) * (ct + 1));
	ct = 0;
	temp = g_global->environ_vars;
	while (temp)
	{
		if (!serv_env_is_internal(temp))
			rt[ct++] = ft_strjoin_f(temp->name, "=", temp->data, "");
		temp = temp->next;
	}
	return (rt);
}
