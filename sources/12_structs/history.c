/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   history.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/03 21:53:36 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/04 17:24:48 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "structs_global.h"
#include "structs_history.h"

/*
**	Structures - history.
**	This function deletes all history entries present.
*/

void			st_hst_clear(t_history_list *head)
{
	t_history_list	*temp;
	t_history_list	*del;

	temp = head;
	while (temp)
	{
		del = temp;
		temp = temp->prev;
		ft_strdel((char**)&(del->line));
		ft_memdel((void**)&del);
	}
	g_global->history = NULL;
	g_global->history_current = NULL;
}

/*
**	Structures - history.
**	This function returns copy of the previous history entry.
**
**	Return values:
**		pointer to the copy of the history entry
**		NULL if no history is available
*/

char			*st_hst_get_prev(void)
{
	char	*rt;

	if (!g_global->history_current)
		g_global->history_current = g_global->history;
	else
		g_global->history_current = g_global->history_current->prev;
	if (g_global->history_current)
		rt = ft_strdup(g_global->history_current->line);
	else
		rt = NULL;
	return (rt);
}

/*
**	Structures - history.
**	This function returns copy of the next history entry.
**
**	Return values:
**		pointer to the copy of the history entry
**		NULL if no history is available
*/

char			*st_hst_get_next(void)
{
	char	*rt;

	if (!g_global->history_current)
	{
		g_global->history_current = g_global->history;
		while (g_global->history_current && g_global->history_current->prev)
			g_global->history_current = g_global->history_current->prev;
	}
	else
		g_global->history_current = g_global->history_current->next;
	if (g_global->history_current)
		rt = ft_strdup(g_global->history_current->line);
	else
		rt = NULL;
	return (rt);
}
