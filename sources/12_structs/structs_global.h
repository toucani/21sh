/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   structs_global.h                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/23 21:42:15 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/05 22:15:24 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef STRUCTS_GLOBAL_H
# define STRUCTS_GLOBAL_H

/*
**	This file provides global_info struct.
**
**	Should be used:
**	everywhere we're working with global_info struct.
**
**	Function naming:
**	st_glb_...
*/

# include <termios.h>
# include <sys/types.h>
# include <sys/ioctl.h>
# include "structs_history.h"
# include "structs_children.h"
# include "structs_environment.h"

# ifndef EMPTY_FILENO
#  define EMPTY_FILENO	-1
# endif

/*
**		Global info structure:
**
**	program_name	|	Program name, we get it from argv[0] and use it
**					|		when printing errors.
**	environ_vars	|	Environment variables.
**	history			|	Hisotory list.
**	history_current	|	Currently selected history entry. Is used when arrowUp
**					|		and arrowDown are pressed.
**	stdin_fd		|	Stdin filedescriptor backup.
**	stdout_fd		|	Stdout filedescriptor backup.
**	stderr_fd		|	Stderr filedescriptor backup.
**	terminal_origin	|	Original terminal settings, are used at_exit to restore
**					|		terminal state.
**	terminal_mine	|	Our teminal settings, we need those to restore terminal
**					|		state after running foreground processes.
**	terminal_size	|	The size of the window, get updated on resize,
**					|		is used in vt_100 functions.
**	terminal_fd		|	Valid terminal file descriptor, it is used while
**					|		forking and reasssigning the terminal to
**					|		foreground processes.
**	sh_pid			|	Pid of the shell.
**	sh_pgid			|	Pgid ot the shell.
**	children		|	We should print info if one of those pids finishes
*/

typedef struct			s_global_info
{
	const char *const	program_name;
	t_env_var			*environ_vars;
	t_history_list		*history;
	t_history_list		*history_current;
	int					stdin_fd;
	int					stdout_fd;
	int					stderr_fd;
	struct termios		terminal_origin;
	struct termios		terminal_mine;
	struct winsize		terminal_size;
	const int			terminal_fd;
	const pid_t			sh_pid;
	const pid_t			sh_pgid;
	t_child_list		*children;
}						t_global_info;

extern t_global_info	*g_global;

#endif
