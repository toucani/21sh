/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   structs_history.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/03 20:35:32 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/04 17:24:53 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef STRUCTS_HISTORY_H
# define STRUCTS_HISTORY_H

/*
**	This file provides history struct, and funtions helpers.
**
**	Should be used:
**	everywhere we're working with global_info struct or history itself.
**
**	Function naming:
**	st_hst_...
*/

/*
**		History structure:
**
**	line	|	The line user entered
*/

typedef struct				s_history_list
{
	const char				*line;
	struct s_history_list	*prev;
	struct s_history_list	*next;
}							t_history_list;

void						st_hst_push_back(const char *const line);
void						st_hst_clear(t_history_list *head);
char						*st_hst_get_prev(void);
char						*st_hst_get_next(void);

#endif
