/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   structs_redirection.h                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/03 14:16:15 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/04 17:16:51 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef STRUCTS_REDIRECTION_H
# define STRUCTS_REDIRECTION_H

/*
**	This file provides redirection struct, and functions helpers.
**
**	Should be used:
**	In structs_command.h, or everywhere we're working with redirections.
**
**	Function naming:
**	st_red_...
*/

typedef enum				e_redir_type
{
	CLOSE,
	WRITE,
	READ,
	APPEND
}							t_redir_type;

/*
**		Redirection structure:
**
**	old_fd		|	FD we will duplicate
**	new_fd		|	FD we will actually use
**	flag		|	Append/Close/Write/Read flag
**	filename	|	If we don't have one of filedescriptors, we will use this
**				|		filename, with flags listed above.
*/

typedef struct				s_redirection
{
	int						old_fd;
	int						new_fd;
	t_redir_type			flag;
	char					*filename;
	struct s_redirection	*next;
}							t_redirection;

t_redirection				*st_red_new(t_command *const token);
t_redirection				*st_red_get_last(const t_command *const token);
void						st_red_delete_all(t_command *const *node);

#endif
