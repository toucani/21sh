/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   environment.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/10 13:41:27 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/04 16:49:52 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "structs_global.h"
#include "structs_environment.h"

/*
**	Structures - environment.
**	This function creates a new env element.
**	Argument is expected to be "NAME=DATA"
**
**	Return value:
**		pointer to a newly created element.
*/

t_env_var	*st_env_new(const char *const full_env)
{
	char		*name;
	t_env_var	*rt;

	name = ft_strsub(full_env, 0, ft_strchr(full_env, '=')
		? (ptrdiff_t)(ft_strchr(full_env, '=') - full_env)
		: (ptrdiff_t)(ft_strlen(full_env)));
	rt = st_env_new_d(name, ft_strchr(full_env, '=')
		? ft_strchr(full_env, '=') + 1 : NULL);
	ft_strdel(&name);
	return (rt);
}

/*
**	Structures - environment.
**	This function creates a new env element.
**
**	Return value:
**		pointer to a newly created element.
*/

t_env_var	*st_env_new_d(const char *const name,
				const char *const data)
{
	t_env_var	*new;

	new = (t_env_var*)ft_memalloc(sizeof(t_env_var));
	if (name)
		new->name = ft_strdup(name);
	if (data)
		new->data = ft_strdup(data);
	return (new);
}

/*
**	Structures - environment.
**	This function properly deletes given element.
*/

void		st_env_delete(t_env_var *const *element)
{
	ft_strdel(&((*element)->name));
	ft_strdel(&((*element)->data));
	ft_memdel((void**)element);
}

/*
**	Structures - environment.
**	This function pushes given element to the list.
*/

void		st_env_push_back(t_env_var *const new)
{
	t_env_var	*temp;

	temp = g_global->environ_vars;
	if (!(g_global->environ_vars))
		g_global->environ_vars = new;
	else
	{
		while (temp->next)
			temp = temp->next;
		temp->next = new;
	}
}

/*
**	Structures - environment.
**	This function looks for an item with the same name.
**
**	Return value:
**		pointer to the element.
**		NULL if not found
*/

t_env_var	*st_env_find(const char *const name)
{
	t_env_var *head;

	head = g_global->environ_vars;
	while (head)
		if (ft_strequ(head->name, name))
			return (head);
		else
			head = head->next;
	return (NULL);
}
