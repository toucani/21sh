/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   history2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/03 16:54:26 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/04 17:43:38 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "defines.h"
#include "structs_global.h"
#include "structs_history.h"

/*
**	Structures - history.
**	This function looks for the last allowed history entry, and deletes
**	all history, which is older than that entry.
*/

static void		delete_history_beyond_limit(void)
{
	t_history_list	*temp;
	size_t			ct;

	ct = 0;
	temp = g_global->history;
	while (temp)
		if (++ct > HISTORY_LIMIT)
			break ;
		else
			temp = temp->prev;
	if (temp)
	{
		temp->next->prev = NULL;
		st_hst_clear(temp);
	}
}

/*
**	Structures - history.
**	This function counts amount of calls it had. If it exceeds HISTORY_LIMIT
**	define, it resets the counter, and deletes history beyond the limit.
**	The counter doesn't reflect the amount of history entries we have.
**	It reflects amount of calls to this function. It is made, not
**	to clear the history every time we add a new record, after reaching
**	HISTORY_LIMIT.
*/

static void		check_history_beyond_limit(void)
{
	static unsigned int	entry_count = 0;

	entry_count++;
	if (entry_count > HISTORY_LIMIT)
	{
		delete_history_beyond_limit();
		entry_count = 0;
	}
}

/*
**	Structures - history.
**	This function adds new history entry to the list, and checks the limit.
*/

void			st_hst_push_back(const char *const line)
{
	t_history_list		*new_record;

	new_record = (t_history_list*)ft_memalloc(sizeof(t_history_list));
	new_record->line = ft_strdup(line);
	if (g_global->history)
		g_global->history->next = new_record;
	new_record->prev = g_global->history;
	g_global->history = new_record;
	g_global->history_current = NULL;
	check_history_beyond_limit();
}
