/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   structs_heredoc.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/03 14:40:40 by dkovalch          #+#    #+#             */
/*   Updated: 2017/09/14 19:56:25 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef STRUCTS_HEREDOC_H
# define STRUCTS_HEREDOC_H

/*
**	This file provides redirection struct, and functions helpers.
**
**	Should be used:
**	In structs_command.h, or everywhere we're working with redirections.
**
**	Function naming:
**	st_hrd_...
*/

typedef struct s_command	t_command;

/*
**		Heredoc structure:
**
**	input			|	Input taken from user, is used in heredocs and
**					|	herestrings. We redirect it into the command.
**	quotmark		|	The quotation mark user typed(if any)
**	stop_string		|	String, which stops recieving input from the user.
**	herestring		|	Means we dont need to read input,
**					|	all input is in stop_string.
*/

typedef struct		s_heredoc
{
	char			*input;
	char			*quotmark;
	char			*stop_string;
	bool			herestring;
}					t_heredoc;

t_heredoc			*st_hrd_create(t_command *const command);
void				st_hrd_delete(t_command *const command);

#endif
