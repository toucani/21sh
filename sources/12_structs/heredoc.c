/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   heredoc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/09 17:45:59 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/04 17:23:34 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "structs_command.h"

/*
**	Structures - heredoc.
**	This function creates new heredoc struct, and puts it into given command.
**
**	Return value:
**		pointer to a newly created structure
**		NULL in case of memory aalocation fail
*/

t_heredoc	*st_hrd_create(t_command *const command)
{
	st_hrd_delete(command);
	command->heredoc = (t_heredoc*)ft_memalloc(sizeof(t_heredoc));
	return (command->heredoc);
}

/*
**	Structures - heredoc.
**	This function properly deltes heredoc node, and its content.
*/

void		st_hrd_delete(t_command *const command)
{
	if (command->heredoc != NULL)
	{
		ft_strdel((&command->heredoc->input));
		ft_strdel(&(command->heredoc->quotmark));
		ft_strdel(&(command->heredoc->stop_string));
		ft_memdel((void**)&(command->heredoc));
	}
}
