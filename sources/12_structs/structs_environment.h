/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   structs_environment.h                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/03 16:44:42 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/04 16:46:36 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef STRUCTS_ENVIRONMENT_H
# define STRUCTS_ENVIRONMENT_H

/*
**	This file provides evironment variables struct, and functions helpers.
**
**	Should be used:
**	everywhere we're working with environment variables or global_info struct.
**
**	Function naming:
**	st_env_...
*/

/*
**		Environment variable structure:
**
**	name	|	name of the variable
**	data	|	variable's data
*/

typedef struct			s_env_var
{
	char				*name;
	char				*data;
	struct s_env_var	*next;
}						t_env_var;

/*
**		Environment variables functions:
**
**	new				->	creates a new node from NAME=VALUE
**	new_d			->	creates a new node from NAME, VALUE
**
**	push_back		->	add given variable to the end of the list
**	delete			->	deletes a single variable object, does not
**							reconnect the list's elements
**	remove			->	removes all variables with given name from the list,
**							and reconnect the nodes
**
**	find			->	looks for a variables with given NAME,
**							returns NULL if the variable is not found
**	get				->	looks for a variables with given NAME,
**							creates the variable, if it is not found
**	get_n_check		->	looks for a variables with given NAME,
**							creates the variable, if it is not found,
**							ensures that the variable has any value,
**							if not - duplicates given value in it
**
**	to_array			->	creates a string array from the variable list
**	is_internal			->	returns true is given variable is internal, a.k.a
**								PROMPT or PROMPT_CLR
*/

t_env_var				*st_env_new(const char *const full_env);
t_env_var				*st_env_new_d(const char *const name,
							const char *const data);

void					st_env_push_back(t_env_var *const new);
void					st_env_delete(t_env_var *const *element);
void					st_env_remove(const char *name);

t_env_var				*st_env_find(const char *const name);
t_env_var				*st_env_get(const char *const name);
t_env_var				*st_env_get_n_check(const char *const name,
							const char *const default_value);

char					**st_env_to_array(void);

#endif
