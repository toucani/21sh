/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   children.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/04 21:02:08 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/04 16:42:53 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "structs_global.h"
#include "structs_children.h"

/*
**	Structures - children
**	This function gives next number to a child prosses.
**	In future - must be syncronized with fg/bg stuff.
**
**	Return value:
**		New child number.
*/

static char		*st_chl_get_new_number(void)
{
	t_child_list	*temp;
	size_t			count;

	count = 1;
	temp = g_global->children;
	while (temp)
	{
		count++;
		temp = temp->next;
	}
	return (ft_itoa(count));
}

/*
**	Structures - children
**	This function creates a new child structure.
**
**	Return value:
**		pointer to newly created structure.
*/

t_child_list	*st_chl_new(const pid_t pid_in, const char *const cmd)
{
	t_child_list	*rt;
	char			*number;
	char			*pid_str;

	pid_str = ft_itoa(pid_in);
	number = st_chl_get_new_number();
	rt = (t_child_list*)ft_memalloc(sizeof(t_child_list));
	rt->pid = pid_in;
	rt->print = true;
	*((char**)&(rt->info)) = ft_strdup("[");
	*((char**)&(rt->info)) = ft_strjoin_ultimate((char**)&(rt->info), &number);
	*((char**)&(rt->info)) = ft_strjoin_del_first((char**)&(rt->info), "] ");
	*((char**)&(rt->info)) = ft_strjoin_ultimate((char**)&(rt->info), &pid_str);
	*((char**)&(rt->command)) = ft_strdup(cmd);
	return (rt);
}

/*
**	Structures - children
**	This function looks for a child structure with given pid.
**
**	Return value:
**		pointer to the structure with corresponding pid.
**		NULL if not found.
*/

t_child_list	*st_chl_find(const pid_t pid_in)
{
	t_child_list	*rt;

	rt = g_global->children;
	while (rt)
		if (rt->pid == pid_in)
			return (rt);
		else
			rt = rt->next;
	return (NULL);
}

/*
**	Structures - children
**	This function adds given child structure to the list.
*/

void			st_chl_push_back(t_child_list *const child)
{
	t_child_list	*temp;

	temp = g_global->children;
	while (temp && temp->next)
		temp = temp->next;
	if (!temp)
		g_global->children = child;
	else
		temp->next = child;
}

/*
**	Structures - children
**	This function deletes child structure with given pid,
**	and reconnects the list.
*/

void			st_chl_delete(const pid_t pid_in)
{
	t_child_list	*prev;
	t_child_list	*temp;

	prev = NULL;
	temp = g_global->children;
	while (temp)
	{
		if (temp->pid == pid_in)
		{
			if (prev)
				prev->next = temp->next;
			else
				g_global->children = temp->next;
			ft_strdel(&(temp->message));
			ft_memdel((void**)&(temp->info));
			ft_memdel((void**)&(temp->command));
			ft_memdel((void**)&temp);
			return ;
		}
		else
		{
			prev = temp;
			temp = temp->next;
		}
	}
}
