/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   command.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/20 13:18:44 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/04 20:46:44 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "structs_command.h"

/*
**	Structures - command.
**	This function creates new command node, and sets raw data into it.
**	raw_data is not copied here!
**
**	Return value:
**		NULL		-	memory allocation fail
**		not NULL	-	success
*/

t_command	*st_cmd_new(const char *raw_data)
{
	t_command	*rt;

	rt = (t_command*)ft_memalloc(sizeof(t_command));
	*((const char**)&(rt->raw_data)) = raw_data;
	return (rt);
}

/*
**	Structures - command.
**	This function properly deletes command node, and its content.
*/

void		st_cmd_delete(t_command *const *node)
{
	size_t		ct;

	ft_memdel((void**)&((*node)->name));
	ft_memdel((void**)&((*node)->raw_data));
	ct = 0;
	while ((*node)->args && (*node)->args[ct])
		ft_memdel((void**)&((*node)->args[ct++]));
	ft_memdel((void**)&((*node)->args));
	if ((*node)->piped_commands)
		st_cmd_delete(&((*node)->piped_commands));
	st_red_delete_all(node);
	st_hrd_delete(*node);
	ft_memdel((void**)node);
}

/*
**	Structures - command.
**	This function deletes given list of commands.
*/

void		st_cmd_delete_list(t_command *const *head)
{
	if (!head || !(*head))
		return ;
	if ((*head)->next != NULL)
		st_cmd_delete_list(&((*head)->next));
	st_cmd_delete(head);
}
