/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   structs_children.h                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/04 20:47:23 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/04 16:42:35 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef STRUCTS_CHILDREN_H
# define STRUCTS_CHILDREN_H

/*
**	This file provides child struct, and functions helpers.
**
**	Should be used:
**	everywhere we're working with children we're interested in.
**
**	Function naming:
**	st_chl_...
*/

# include <stdbool.h>
# include <sys/types.h>

/*
**			Children structure:
**
**	pid			|	Process id we're interested in
**	info		|	Number and pid, used only in printing
**	command		|	Full path to the command, used only in printing
**	message		|	Mesage to print
**	remove		|	True - if we can delete current item
**	print		|	True - if we have updated the message, and should print it
*/

typedef struct			s_child_list
{
	pid_t				pid;
	const char *const	info;
	const char *const	command;
	char				*message;
	bool				remove;
	bool				print;
	struct s_child_list	*next;
}						t_child_list;

t_child_list			*st_chl_new(const pid_t pid_in, const char *const cmd);
t_child_list			*st_chl_find(const pid_t pid_in);
void					st_chl_push_back(t_child_list *const child);
void					st_chl_delete(const pid_t pid_in);

#endif
