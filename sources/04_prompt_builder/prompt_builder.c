/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prompt_builder.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/23 21:40:01 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/03 21:54:57 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "defines.h"
#include "ft_printf.h"
#include "structs_environment.h"

/*
**	Loop runner
**	This function processes and prints the prompt.
*/

void		pb_print_prompt(void)
{
	t_env_var		*prompt;
	t_env_var		*prompt_clr;

	prompt = st_env_get_n_check(DEFAULT_PROMPT_VAR, DEFAULT_PROMPT);
	prompt_clr =
		st_env_get_n_check(DEFAULT_PROMPT_CLR_VAR, DEFAULT_PROMPT_CLR);
	ft_printf("%r%s%r", prompt_clr->data, prompt->data, "reset");
}
