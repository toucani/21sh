/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_actions_tab.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/10 18:20:24 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/01 22:39:10 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "error.h"
#include "string.h"
#include "string_array.h"
#include "prompt_builder.h"
#include "cursor_movements.h"
#include "key_actions_internal.h"
#include "line_reader_structure.h"

/*
**	Key action
**	This function only prints an error message.
*/

static void		print_error(const char *const current, const char *const line)
{
	err_print("\n"ERR_PARSE_AT, current);
	cm_bell();
	pb_print_prompt();
	ft_putstr(line);
	cm_global_init();
}

/*
**	Key action
**	This function checks if there are no unsupported characters in the line,
**	we're going to work with.
*/

static bool		no_errors(const char *const line)
{
	return (!((ft_strchr(line, '*') != ft_strrchr(line, '*'))
	|| (ft_strchr(line, '*') && ft_strchr(line, '?'))
	|| ft_strchr(line, '\"') || ft_strchr(line, '\'')
	|| ft_strchr(line, '`') || ft_strchr(line, '$')));
}

/*
**	Key action
**	This function gets right line to work with, checks it for errors, and
**	calles next functions, if everything is ok, or prints an error message.
*/

void			ka_tab_action(t_line *const sline)
{
	const char	**words;
	const char	*current;
	const char	*asterics;
	size_t		ct;

	words = (const char **)str_split_into_words(sline->line);
	ct = str_arr_length(words);
	asterics = ft_strdup("*");
	current = (ct > 0 && !ft_isspc(sline->line[sline->line_len - 1]))
		? words[ct - 1] : asterics;
	if (no_errors(current))
		ka_do_tab_action(sline, current, (ct - ((ct > 0
			&& !ft_isspc(sline->line[sline->line_len - 1])) ? 1 : 0)) == 0);
	else
		print_error(current, sline->line);
	ft_strdel((char**)&asterics);
	ct = 0;
	while (words && words[ct] != NULL)
		ft_strdel((char**)&(words[ct++]));
	ft_memdel((void**)&(words));
}
