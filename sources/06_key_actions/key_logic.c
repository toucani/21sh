/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_logic.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/01 18:49:08 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/05 16:13:22 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "structs_global.h"
#include "cursor_movements.h"
#include "line_reader_structure.h"

/*
**	Key actions
**	This function is a controller between t_line model and terminal view.
**	This function doesn't move the cursor, it only changes global cursor
**	position struct values. You should use cm_cursor_apply by yourself
**	to apply these changes.
*/

void	ka_cursor_left(t_line *sline)
{
	char	*previous;

	if (sline->line[--sline->position] == '\n')
	{
		previous = sline->line;
		while (ft_strchr(previous + 1, '\n') < &(sline->line[sline->position]))
			previous = ft_strchr(previous + 1, '\n');
		cm_cursor_add_offset(0, -(g_global->terminal_size.ws_col
			- ft_strlen_dl(previous + 1, '\n')));
		if (previous == sline->line)
			cm_cursor_add_offset(0, sline->px_offset);
	}
	else
		cm_cursor_add_offset(0, -1);
}

/*
**	Key actions
**	This function is a controller between t_line model and terminal view.
**	This function doesn't move the cursor, it only changes global cursor
**	position struct values. You should use cm_cursor_apply by yourself
**	to apply these changes.
**
**	N.B: Do we really need this check
**		[...&& sline->line[sline->position + 1] != '\0']?
*/

void	ka_cursor_right(t_line *sline)
{
	cm_cursor_add_offset(0, 1);
	if (sline->line[sline->position] == '\n'
		&& sline->line[sline->position + 1] != '\0')
		cm_cursor_add_offset(1, -(g_cur_pos->col - 1));
	sline->position++;
}
