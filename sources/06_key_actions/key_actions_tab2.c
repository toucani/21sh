/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_actions_tab2.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/10 18:20:24 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/06 16:01:55 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "service.h"
#include "string_array.h"
#include "structs_environment.h"
#include "key_actions_internal.h"
#include "line_reader_structure.h"

/*
**	Key action
**	This function return a null terminated array of the paths, where we should
**	look for the substitutions.
**
**	Return values:
**	If path is not specified in the line (aka "dir/dir/item"), and it's
**	not a command, returns ".".
*/

static char	**get_tab_paths(const char *const line, const bool command)
{
	char	**rt;
	char	*temp;

	if ((!line || !(temp = ft_strrchr(line, '/'))) && command)
		return (ft_strsplit(st_env_get("PATH")->data, ':'));
	else
	{
		rt = ft_memalloc(sizeof(char*) * 2);
		rt[0] = (temp != NULL) ? ft_strsub(line, 0, temp - line)
			: ft_strdup(".");
		return (rt);
	}
}

/*
**	Key action
**	This function returns the name, we should try to match.
**	The name will have an astericks.
*/

static char	*get_tab_name(const char *const line)
{
	char	*temp;

	temp = ft_strrchr(line, '/');
	temp = ft_strdup(temp ? temp + 1 : line);
	if (!ft_strchr(temp, '*') && !ft_strchr(temp, '?'))
		return (ft_strjoin_del_first(&temp, "*"));
	return (temp);
}

/*
**	Key action
**	This function gets all possible matches for the path and name we have.
**
**	Return value:
**	Null terminated array of all possible matches found.
*/

static char	**get_all_matches(const char *const line, const bool is_command)
{
	size_t				ct;
	const char			*name;
	const char			**paths;
	const char *const	*result;

	ct = 0;
	result = (const char*const*)ft_memalloc(sizeof(char**));
	name = get_tab_name(line);
	paths = (const char**)get_tab_paths(line, is_command);
	while (paths && paths[ct] != NULL)
	{
		if (serv_can_access(paths[ct], R_OK))
			result = str_arr_concat_arrays(result,
				(const char**)serv_dir_get_matches(paths[ct], name), true);
		ft_strdel((char**)&(paths[ct++]));
	}
	ft_memdel((void**)&paths);
	ft_strdel((char**)&name);
	str_arr_sort((const char**)result);
	return ((char**)result);
}

/*
**	Key action
**	This function gets possible matches, and deletes them after we used them.
*/

void		ka_do_tab_action(t_line *const sline,
				const char *const line, const bool is_command)
{
	char	**results;
	size_t	ct;

	ct = 0;
	results = get_all_matches(line, is_command);
	ka_really_do_tab_action((const char**)results, sline);
	while (results && results[ct] != NULL)
		ft_strdel((char**)&(results[ct++]));
	ft_memdel((void**)&(results));
}
