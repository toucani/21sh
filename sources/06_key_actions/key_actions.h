/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_actions.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/21 20:58:34 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/03 20:53:15 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef KEY_ACTIONS_H
# define KEY_ACTIONS_H

/*
**	This file provides access to keys actions functions.
**
**	Should be used:
**	'line reader' section of the program only.
**
**	Function naming:
**	ka_...
*/

typedef struct s_line	t_line;

void	ka_history_actions(char *const symbol, t_line *const sline,
			const bool emulate) __attribute__((nonnull));
void	ka_line_actions(char *const symbol, t_line *const
			sline) __attribute__((nonnull));
void	ka_key_actions(const char *const symbol, t_line *const
			sline) __attribute__((nonnull));

#endif
