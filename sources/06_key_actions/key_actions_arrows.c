/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_actions_arrows.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/10 18:20:24 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/03 20:52:00 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "key_codes.h"
#include "cursor_movements.h"
#include "key_actions_internal.h"
#include "line_reader_structure.h"

/*
**	Key action
**	This function moves the caret thru the line,
**	if there is room, where to move.
*/

static void	key_left(t_line *const sline)
{
	if (sline->position > 0)
		ka_cursor_left(sline);
	else
		cm_bell();
}

/*
**	Key action
**	This function moves the caret thru the words in the line.
*/

static void	key_opt_left(t_line *const sline)
{
	if (sline->position == 0)
	{
		cm_bell();
		return ;
	}
	ka_cursor_left(sline);
	while (sline->position > 0 && ft_isspc(sline->line[sline->position]))
		ka_cursor_left(sline);
	while (sline->position > 0 && !ft_isspc(sline->line[sline->position])
	&& !ft_isspc(sline->line[sline->position - 1]))
		ka_cursor_left(sline);
}

/*
**	Key action
**	This function moves the caret thru the line,
**	if there is room, where to move.
*/

static void	key_right(t_line *const sline)
{
	if (sline->position < sline->line_len)
		ka_cursor_right(sline);
	else
		cm_bell();
}

/*
**	Key action
**	This function moves the caret thru the words in the line.
*/

static void	key_opt_right(t_line *const sline)
{
	if (sline->position >= sline->line_len)
	{
		cm_bell();
		return ;
	}
	while (sline->position < sline->line_len
	&& !ft_isspc(sline->line[sline->position]))
		ka_cursor_right(sline);
	while (sline->position < sline->line_len
	&& ft_isspc(sline->line[sline->position])
	&& ft_isspc(sline->line[sline->position + 1]))
		ka_cursor_right(sline);
	if (sline->position < sline->line_len)
		ka_cursor_right(sline);
}

/*
**	Key action
**	This function decides what to call next, depending on the symbol we have.
*/

void		ka_arrows_action(const char *const symbol, t_line *const sline)
{
	if (ft_strequ(symbol, CUR_LF))
		key_left(sline);
	else if (ft_strequ(symbol, CUR_RH))
		key_right(sline);
	else if (ft_strequ(symbol, OPT_CUR_LF))
		key_opt_left(sline);
	else if (ft_strequ(symbol, OPT_CUR_RH))
		key_opt_right(sline);
	cm_cursor_apply();
}
