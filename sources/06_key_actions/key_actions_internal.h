/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_actions_internal.h                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/23 11:06:50 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/03 20:52:54 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef KEY_ACTIONS_INTERNAL_H
# define KEY_ACTIONS_INTERNAL_H

/*
**	This file provides access to internal key actions functions.
**
**	Should be used:
**	'key actions' section of the program only.
*/

typedef struct s_line	t_line;

void	ka_arrows_action(const char *const symbol, t_line *const
			sline) __attribute__((nonnull));
void	ka_tab_action(t_line *const sline) __attribute__((nonnull));
void	ka_do_tab_action(t_line *const sline, const char *const line,
			const bool is_command) __attribute__((nonnull));
void	ka_really_do_tab_action(const char **results, t_line
			*sline) __attribute__((nonnull));
void	ka_tab_print_in_columns(const char *const *const
			results) __attribute__((nonnull));

void	ka_cursor_left(t_line *sline);
void	ka_cursor_right(t_line *sline);

#endif
