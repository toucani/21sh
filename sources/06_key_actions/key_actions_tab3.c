/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_actions_tab3.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/23 20:14:52 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/01 22:47:08 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "string.h"
#include "service.h"
#include "string_array.h"
#include "prompt_builder.h"
#include "cursor_movements.h"
#include "key_actions_internal.h"
#include "line_reader_structure.h"

/*
**	Key action
**	This function checks if the last argument is a folder, and adds a slash
**	if it is.
*/

static void		add_backslash(t_line *sline)
{
	const char	**words;
	size_t		ct;

	words = (const char**)str_split_into_words(sline->line);
	if (serv_is_dir(words[str_arr_length(words) - 1]))
		sline->line = ft_strjoin_del_first(&(sline->line), "/");
	ct = 0;
	while (words && words[ct] != NULL)
		ft_strdel((char**)&(words[ct++]));
	ft_memdel((void**)&words);
}

/*
**	Key action
**	This function looks for the substring we were matching, and
**	substites it with the new one.
*/

static void		substitute(t_line *const sline, const char *const new)
{
	const char		last = sline->line[sline->line_len - 1];
	const size_t	len = sline->line_len;
	size_t			ct;

	ct = MAX((ssize_t)(len - ft_strlen(new)), 0);
	while (ct < len)
	{
		if (ft_strnequ(&(sline->line[ct]), new, len - ct -
			((last == '*' || last == '?') ? 1 : 0)))
			break ;
		ct++;
	}
	sline->line[ct] = '\0';
	sline->line = ft_strjoin_del_first(&(sline->line), new);
}

/*
**	Key action
**	This function tries to find a substring, common for all strings
**	in the array. It get the widest string in the array first, and then,
**	comparing it with other strings, cut it to the common substring,
**	untils reaches array's end or the length reaches 0.
**
**	Return value:
**	longes common substring, or NULL if not found
*/

static char		*get_similar(const char **results)
{
	size_t	current;
	size_t	ct;
	char	*similar;

	current = 0;
	similar = NULL;
	while (results[current++] != NULL)
		if (ft_strlen(results[current - 1]) > ft_strlen(similar))
			similar = (char*)(results[current - 1]);
	similar = ft_strdup(similar);
	current = 0;
	while (results[current] != NULL && ft_strlen(similar) > 0)
	{
		ct = 0;
		while (similar[ct] != '\0' && results[current][ct] != '\0'
		&& similar[ct] == results[current][ct])
			ct++;
		similar[ct] = '\0';
		current++;
	}
	if (ft_strlen(similar) == 0)
		ft_strdel(&similar);
	return (similar);
}

/*
**	Key action
**	This function decides what to do, depending on given arguments.
*/

void			ka_really_do_tab_action(const char **results, t_line *sline)
{
	const char *const	similar = get_similar(results);
	const size_t		amount = str_arr_length(results);

	if (amount == 1 || similar)
	{
		substitute(sline, similar ? similar : results[0]);
		add_backslash(sline);
		while (sline->position > 0)
			ka_cursor_left(sline);
		cm_cursor_apply();
	}
	if (amount > 1)
	{
		ka_tab_print_in_columns(results);
		pb_print_prompt();
	}
	if (amount != 1)
		cm_bell();
	if (amount == 0)
		return ;
	ft_strdel((char**)&similar);
	sline->position = ft_strlen(sline->line);
	ft_putstr(sline->line);
	cm_global_init();
}
