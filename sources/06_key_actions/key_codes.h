/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_codes.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/21 20:54:24 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/03 20:53:38 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef KEY_CODES_H
# define KEY_CODES_H

/*
**	This file provides key codes defines.
**
**	Should be used:
**	'key actions' sections of the program.
*/

# define TAB			"\x09"
# define CTRL_A			"\x01"
# define CTRL_C			"\x03"
# define CTRL_D			"\x04"
# define CTRL_E			"\x05"
# define CTRL_L			"\x0c"
# define CTRL_N			"\x0e"
# define CTRL_P			"\x10"
# define CTRL_U			"\x15"
# define BASCKP			"\x7f"
# define DELETE			"\x1b\x5b\x33\x7e"
# define END			"\x1b\x5b\x46"
# define HOME			"\x1b\x5b\x48"
# define CUR_UP			"\x1b\x5b\x41"
# define CUR_DW			"\x1b\x5b\x42"
# define CUR_RH			"\x1b\x5b\x43"
# define CUR_LF			"\x1b\x5b\x44"
# define OPT_CUR_RH		"\x1b\x66"
# define OPT_CUR_LF		"\x1b\x62"

#endif
