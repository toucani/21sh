/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_actions_basic.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/22 22:14:48 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/05 14:51:47 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "key_codes.h"
#include "structs_global.h"
#include "cursor_movements.h"
#include "key_actions_internal.h"
#include "line_reader_structure.h"

/*
**	Key action
**	This function removes a symbol to the left of the caret.
*/

static void	backspace_symbol(t_line *const sline)
{
	size_t	target_position;

	if (sline->position > 0)
	{
		ka_cursor_left(sline);
		cm_cursor_apply();
		ft_strcpy(&(sline->line[sline->position]),
			&(sline->line[sline->position + 1]));
		sline->line_len--;
		cm_screen_clear(CLEAR_FROM_HERE);
		target_position = sline->line_len - sline->position;
		cm_print(&(sline->line[sline->position]));
		sline->position = sline->line_len;
		while (target_position--)
			ka_cursor_left(sline);
		cm_cursor_apply();
	}
	else
		cm_bell();
}

/*
**	Key action
**	This function removes a symbol right under the caret.
*/

static void	delete_symbol(t_line *const sline)
{
	size_t	target_position;

	if (sline->position < sline->line_len)
	{
		ft_strcpy(&(sline->line[sline->position]),
			&(sline->line[sline->position + 1]));
		sline->line_len--;
		cm_screen_clear(CLEAR_FROM_HERE);
		target_position = sline->line_len - sline->position;
		cm_print(&(sline->line[sline->position]));
		sline->position = sline->line_len;
		while (target_position--)
			ka_cursor_left(sline);
		cm_cursor_apply();
	}
	else
		cm_bell();
}

/*
**	Key action
**	This function inserts a new line, by scrolling, if we are inserting symbols
**	on the last line, and printing the resulting line will hit the
**	edge of the screen. Is used only in insert_symbol();
*/

static void	print_new_line(const t_line *const sline, const int symbol_len)
{
	char		*new_line;
	int			real_chars_amount;
	bool		affects_last_line;
	unsigned	move_am;

	new_line = ft_strrchr(sline->line, '\n');
	real_chars_amount = (new_line ? ft_strlen(new_line + 1)
		: sline->line_len + symbol_len) + (new_line ? 0 : sline->px_offset);
	affects_last_line = g_cur_pos->line == g_global->terminal_size.ws_row
		|| (g_cur_pos->line + (real_chars_amount /
			g_global->terminal_size.ws_col) >= g_global->terminal_size.ws_row);
	if (affects_last_line
	&& ((real_chars_amount - 1) % g_global->terminal_size.ws_col) == 0)
	{
		move_am = g_global->terminal_size.ws_row - g_cur_pos->line + 1;
		cm_cursor_move_down(move_am);
		cm_cursor_move_up(move_am);
	}
}

/*
**	Key action
**	This function inserts symbols into the line.
**	This function add an empty line, by scrolling down.
*/

static void	insert_symbol(const char *const symbol, t_line *const sline)
{
	char	*head;
	char	*tail;
	size_t	skip_symbols;

	head = ft_strsub(sline->line, 0, sline->position);
	tail = ft_strdup(&(sline->line[sline->position]));
	skip_symbols = ft_strlen(tail);
	ft_strdel(&(sline->line));
	sline->line = ft_strjoin_del_first(&head, symbol);
	sline->line = ft_strjoin_ultimate(&(sline->line), &tail);
	cm_screen_clear(CLEAR_FROM_HERE);
	print_new_line(sline, ft_strlen(symbol));
	cm_print(&(sline->line[sline->position]));
	sline->line_len += ft_strlen(symbol);
	sline->position = sline->line_len;
	while (skip_symbols--)
		ka_cursor_left(sline);
	cm_cursor_apply();
}

/*
**	Key action
**	This function decides what to call next. It always performs an action!
*/

void		ka_key_actions(const char *const symbol, t_line *const sline)
{
	if (ft_strequ(symbol, CUR_RH) || ft_strequ(symbol, CUR_LF)
	|| ft_strequ(symbol, OPT_CUR_RH) || ft_strequ(symbol, OPT_CUR_LF))
		ka_arrows_action(symbol, sline);
	else if (ft_strequ(symbol, BASCKP))
		backspace_symbol(sline);
	else if (ft_strequ(symbol, DELETE))
		delete_symbol(sline);
	else if (ft_strequ(symbol, CTRL_D))
		cm_bell();
	else if (sline->position < sline->line_len)
		insert_symbol(symbol, sline);
	else
	{
		sline->line = ft_strjoin_del_first(&(sline->line), symbol);
		sline->position = ft_strlen(sline->line);
		cm_print(symbol);
	}
}
