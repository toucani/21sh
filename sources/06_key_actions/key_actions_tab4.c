/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_actions_tab4.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/23 21:21:39 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/06 16:16:32 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "ft_printf.h"
#include "structs_global.h"
#include "cursor_movements.h"

/*
**	Key action
**	This function prints the question, and collects the answer from the user.
**
**	Return value:
**		true	-	we should print the stuff
**		false	-	we shouldn print it
*/

static bool		ask(const size_t lines, const size_t amount)
{
	char	answ;

	answ = 'y';
	ft_printf("\nDisplay all %zu options(± %zu lines)? [y/n]: ", amount, lines);
	if (read(STDIN_FILENO, &answ, 1) < 1)
		return (true);
	ft_putchar('\n');
	cm_line_clear(CLEAR_FROM_HERE);
	return (answ == 'y' || answ == 'Y');
}

/*
**	Key action
**	This function counts the widest element, total lines and columns amount.
*/

static size_t	count(size_t *columns, size_t *lines, size_t *width,
					const char *const *const results)
{
	size_t	ct;

	if (results == NULL)
		return (0);
	ct = 0;
	*width = 0;
	while (results[ct++] != NULL)
		if (ft_strlen(results[ct - 1]) > *width)
			*width = ft_strlen(results[ct - 1]);
	*columns = MAX(g_global->terminal_size.ws_col / (*width + 2), 1);
	*lines = (ct / *columns) + ((ct % *columns) ? 1 : 0);
	return (ct);
}

/*
**	Key action
**	This function prints given array in columns, using the widest element
**	as column's width. Before printing it askes the user, if we have more
**	than 10 lines to print(see static bool ask).
**
**	result_ct is used as a total count of the items for function ask
*/

void			ka_tab_print_in_columns(const char *const *const results)
{
	size_t	width;
	size_t	columns;
	size_t	lines;
	size_t	result_ct;

	lines = 0;
	width = 0;
	columns = 0;
	result_ct = count(&columns, &lines, &width, results);
	if (lines <= 10 || ask(lines, result_ct))
	{
		if (lines <= 10)
			ft_putchar('\n');
		result_ct = 0;
		while (results[result_ct++])
			ft_printf("%-*s%s", width, results[result_ct - 1],
				(result_ct % columns == 0 && results[result_ct] != NULL)
					? "\n" : "  ");
	}
	ft_putchar('\n');
}
