/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   line_actions.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/22 22:14:51 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/03 20:58:00 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "key_codes.h"
#include "prompt_builder.h"
#include "cursor_movements.h"
#include "key_actions_internal.h"
#include "line_reader_structure.h"

/*
**	Key action
**	This function moves caret to the beginning of the line.
*/

static void	key_home(t_line *const sline)
{
	if (sline->position > 0)
	{
		while (sline->position > 0)
			ka_cursor_left(sline);
		cm_cursor_apply();
	}
	else
		cm_bell();
}

/*
**	Key action
**	This function moves caret to the end of the line.
*/

static void	key_end(t_line *const sline)
{
	if (sline->position < sline->line_len)
	{
		while (sline->position < sline->line_len)
			ka_cursor_right(sline);
		cm_cursor_apply();
	}
	else
		cm_bell();
}

/*
**	Key action
**	This function clears the screen. Normally we would exit
**	line reading mode after that.
**
**	This function contains a bug. It shouldn't print promt if it was
**	called from heredoc's functions, or whe the user forgot to add
**	some quotations marks. But this function doesn't know about it...
**	There must be a method(callback) to print desired string...
*/

static void	clear_screen(t_line *const sline)
{
	cm_screen_erase();
	pb_print_prompt();
	ft_putstr(sline->line);
	sline->position = sline->line_len;
	cm_global_init();
}

/*
**	Key action
**	This function clears the line up to beginning.
*/

static void	clear_line(t_line *const sline)
{
	key_home(sline);
	cm_screen_clear(CLEAR_FROM_HERE);
	ft_strclr(sline->line);
}

/*
**	Key action
**	This function decides what to call next. If we've found a match to
**	the symbol, symbol is being cleared, to prevent further processing.
*/

void		ka_line_actions(char *const symbol, t_line *const sline)
{
	if (ft_strequ(symbol, HOME) || ft_strequ(symbol, CTRL_A))
		key_home(sline);
	else if (ft_strequ(symbol, END) || ft_strequ(symbol, CTRL_E))
		key_end(sline);
	else if (ft_strequ(symbol, CTRL_L))
		clear_screen(sline);
	else if (ft_strequ(symbol, CTRL_U))
		clear_line(sline);
	else if (ft_strequ(symbol, TAB))
		ka_tab_action(sline);
	else
		return ;
	ft_strclr(symbol);
}
