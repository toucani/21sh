/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   history_actions.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/22 23:04:29 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/03 20:57:47 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "key_codes.h"
#include "structs_history.h"
#include "cursor_movements.h"
#include "key_actions_internal.h"
#include "line_reader_structure.h"

/*
**	Key action
**	This function changes the line into the history entry.
**	It moves cursor to the beginning of the line, before clearing and
**	printing not to get artifacts on the screen.
*/

static void	new_history_line(t_line *const sline, const bool prev)
{
	char	*history_entry;

	while (sline->position > 0)
		ka_cursor_left(sline);
	cm_cursor_apply();
	cm_screen_clear(CLEAR_FROM_HERE);
	history_entry = prev ? st_hst_get_prev() : st_hst_get_next();
	cm_print(history_entry);
	ft_strdel(&(sline->line));
	sline->line = history_entry;
	sline->position = ft_strlen(history_entry);
}

/*
**	Key action
**	This function moves the caret up and down in the multiline line.
**
**	needa_move_more flag is used to move the cursor not only to the nearest '\n'
**	but move it really down, in case when the user puts the cursor in the middle
**	of the line in multiline history, and presses down arrow.
*/

static void	move_up_down(t_line *const sline, const bool move_up)
{
	const bool	needa_move_more = sline->line[sline->position] != '\n';

	if (!move_up && sline->position < sline->line_len)
		ka_cursor_right(sline);
	if (move_up && sline->position > 0)
		ka_cursor_left(sline);
	while (sline->position > 0 && sline->position < sline->line_len
	&& sline->line[sline->position] != '\n')
		move_up ? ka_cursor_left(sline) : ka_cursor_right(sline);
	if (!move_up && needa_move_more)
	{
		ka_cursor_right(sline);
		while (sline->position < sline->line_len
		&& sline->line[sline->position] != '\n')
			ka_cursor_right(sline);
	}
	cm_cursor_apply();
}

/*
**	Key action
**	This function takes nesessary action after the key press,
**	depending on the symbol we have. If we've found a match to the symbol,
**	symbol is being cleared, to prevent further processing.
*/

void		ka_history_actions(char *const symbol, t_line *const sline,
				const bool emulate)
{
	char	*first_n;
	char	*last_n;

	first_n = ft_strchr(sline->line, '\n');
	last_n = ft_strrchr(sline->line, '\n');
	if (ft_strequ(symbol, CTRL_P) || ft_strequ(symbol, CTRL_N)
	|| (ft_strequ(symbol, CUR_UP)
		&& (!first_n || (ptrdiff_t)(sline->position)
		<= ABS((ptrdiff_t)(first_n - sline->line))))
	|| (ft_strequ(symbol, CUR_DW)
		&& (!last_n || (ptrdiff_t)(sline->position)
		> ABS((ptrdiff_t)(last_n - sline->line)))))
		emulate ? ft_strclr(symbol) : new_history_line(sline,
			ft_strequ(symbol, CTRL_P) || ft_strequ(symbol, CUR_UP));
	else if (ft_strequ(symbol, CUR_DW) || ft_strequ(symbol, CUR_UP))
		emulate ? ft_strclr(symbol)
			: move_up_down(sline, ft_strequ(symbol, CUR_UP));
	else
		return ;
	ft_strclr(symbol);
}
