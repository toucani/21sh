/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   line_reader.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/03 17:23:59 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/03 20:45:42 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LINE_READER_H
# define LINE_READER_H

/*
**	This file provides access to line reader functions.
**	Main entry point is: lr_read_the_line().
**
**	Should be used:
**	'loop runner', 'line processor'(for heredocs/herestrings)
**		sections of the program.
**
**	Function naming:
**	lr_...
*/

# include <stdbool.h>

/*
**	lr_get_line return codes.
**	If, while reading the line, we got:
**		Ctrl+C symbol	-	we immideately return LR_CTRL_C code
**		Ctrl+D symbol	-	if it is in the beginning of the line,
**							we immideately return LR_CTRL_D code
**		other symbols	-	we return when we got new line symbol,
**							with LR_N_LINE code.
*/

typedef enum	e_lr_ret_code
{
	LR_NONE,
	LR_N_LINE,
	LR_CTRL_C,
	LR_CTRL_D
}				t_lr_ret_code;

bool			lr_get_input(char **line) __attribute__((nonnull));
t_lr_ret_code	lr_get_line(char **line, const bool
					use_history) __attribute__((nonnull));

#endif
