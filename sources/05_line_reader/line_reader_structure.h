/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   line_reader_structure.h                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/21 22:12:43 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/03 21:57:31 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LINE_READER_STRUCTURE_H
# define LINE_READER_STRUCTURE_H

/*
**	This file provides access to line reader structure.
**
**	Should be used:
**	'line reader' and 'key_actions' sections of the program only.
*/

# include <inttypes.h>

/*
**		Line reader structure
**
**	line		|	The line we read from the user
**	position	|	Current caret position in the line
**	line_len	|	Length of the line, gets updated after every key press
**				|	in "lr_key_take_action" function. Is used not to call
**				|	ft_strlen very often, in different functions.
**	px_offset	|	Prompt x offset(initial position of the cursor, before
**				|	the user starts to enter anything)
*/

typedef struct		s_line
{
	char			*line;
	size_t			position;
	size_t			line_len;
	const size_t	px_offset;
}					t_line;

#endif
