/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   line_getter.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/21 22:54:34 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/06 18:42:11 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "string.h"
#include "key_codes.h"
#include "key_actions.h"
#include "line_reader.h"
#include "structs_global.h"
#include "cursor_movements.h"
#include "line_reader_structure.h"

/*
**	Line reader
**	This function checks all sto conditions, and returns right code.
**
**	Return value:
**		Return code, corresponding to the symbol we have.
*/

static t_lr_ret_code	stop_condition(const char *const line,
							const char *const symbol)
{
	if (*symbol == '\n')
		return (LR_N_LINE);
	else if ((line == NULL || line[0] == '\0') && ft_strequ(symbol, CTRL_D))
		return (LR_CTRL_D);
	else if (ft_strequ(symbol, CTRL_C))
		return (LR_CTRL_C);
	return (LR_NONE);
}

/*
**	Line reader
**	This function reacts to every symbol we read.
**	This function changes every tab symbol to a single space, before
**	inserting these symbols into the line. Just to be sure that the user
**	is not able to paste a line with tabs into the app.
**
**	This function contains a bug fix. It returns if the symbol is Ctrl + L
**	and we've turned history off. Currently we do this only when we are
**	getting line with PS2 in front(heredoc and not finished quotes modes).
**	And we dont need to use Ctrl + L in these modes, since using it prints
**	promt, after clearing the screen, instead of printing PS2.
*/

static void				act(char *const symbol, t_line *const sline,
							const bool use_history)
{
	size_t	ct;

	ka_history_actions(symbol, sline, !use_history);
	if (!use_history && (ft_strequ(symbol, CTRL_L) || ft_strequ(symbol, TAB)))
		return ;
	ka_line_actions(symbol, sline);
	ct = 0;
	while (symbol[ct++] != '\0')
		symbol[ct - 1] = symbol[ct - 1] == '\t' ? ' ' : symbol[ct - 1];
	if (symbol[0] != '\0')
		ka_key_actions(symbol, sline);
	sline->line_len = ft_strlen(sline->line);
}

/*
**	Line reader
**	This function turns on or off generating signals with special keys.
*/

static void				set_terminal(const bool disable)
{
	struct termios	term;

	tcgetattr(g_global->terminal_fd, &term);
	if (disable)
		term.c_lflag &= ~ISIG;
	else
		term.c_lflag |= ISIG;
	tcsetattr(g_global->terminal_fd, TCSANOW, &term);
}

/*
**	Line reader
**	This function reads the user input and reacts to keys.
**	It returns if newline was read, or EOF was the first symbol in the line.
**
**	Return value:
**		Return code, corresponding to the stop symbol we have.
*/

t_lr_ret_code			lr_get_line(char **line, const bool use_history)
{
	t_lr_ret_code	return_code;
	char			symbol[9];
	t_line			sline;

	cm_global_init();
	ft_bzero(&sline, sizeof(t_line));
	*((size_t*)(&(sline.px_offset))) = g_cur_pos->col;
	ft_bzero(symbol, 9);
	set_terminal(true);
	while (read(STDIN_FILENO, symbol, 8))
	{
		if ((return_code = stop_condition(sline.line, symbol)) != LR_NONE)
			break ;
		act(symbol, &sline, use_history);
		ft_bzero(symbol, 8);
	}
	ft_strcpy(symbol, END);
	if (sline.position < sline.line_len)
		act(symbol, &sline, use_history);
	cm_new_line();
	set_terminal(false);
	cm_global_clear();
	*line = sline.line;
	return (return_code);
}
