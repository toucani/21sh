/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   input_getter.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/21 22:54:30 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/03 20:45:04 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of 21sh project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include "string.h"
#include "grammar.h"
#include "defines.h"
#include "line_reader.h"
#include "structs_environment.h"

/*
**	Line reader
**	This function prints an error message if we don't have equal amount
**	of quotation marks in the line.
*/

static void	print_qt_error(const char qt)
{
	if (gr_is_sqt(&qt, 0))
		ft_putstr("single ");
	else if (gr_is_dqt(&qt, 0))
		ft_putstr("double ");
	else if (gr_is_bqt(&qt, 0))
		ft_putstr("back ");
	ft_putstr("quote");
}

/*
**	Line reader
**	This function checks if the amount of quotation marks in
**	the line, we have read, is even. Prints message, if it is odd.
**
**	Return value:
**		true	-	it is even
**		false	-	it is odd
*/

static bool	is_qt_amount_even(const char *const input)
{
	const char	*last_qt;
	size_t		ct;

	ct = 0;
	while (input != NULL && input[ct])
	{
		if (gr_is_qt(input, ct))
		{
			last_qt = str_skip_qts(&input[ct]);
			if (!last_qt)
			{
				print_qt_error(input[ct]);
				return (false);
			}
			ct = last_qt - input;
		}
		else
			ct++;
	}
	return (true);
}

/*
**	Line reader
**	This function checks whether we need to get more input from user.
**
**	Return value:
**		true	-	we need to read more
**		false	-	no need to read more
*/

static bool	needa_read_more(const char *const line)
{
	size_t	ct;

	if (line == NULL || (ct = ft_strlen(line)) == 0)
		return (false);
	ct--;
	if ((gr_is_bsl(line, ct) && (ct == 0 || !gr_is_bsl(line, ct - 1)))
	|| (!is_qt_amount_even(line)))
		return (true);
	while (ct > 0 && ft_isspc(line[ct]))
		ct--;
	if ((gr_is_separator(line, ct) && !gr_is_background(line, ct))
	|| gr_is_redirection(line, ct))
		return (true);
	return (false);
}

/*
**	Line reader
**	This function add a new line character, if there is no backslash.
**
**	Return value:
**		true	-	no EOF
**		false	-	we've encountered EOF as the first symbol in the line
*/

static void	fizalize_the_line(char **line)
{
	if (gr_is_bsl(*line, ft_strlen(*line) - 1)
	&& !gr_has_esc_char(*line, ft_strlen(*line) - 1))
		(*line)[ft_strlen(*line) - 1] = 0;
	else
		(*line) = ft_strjoin_del_first(line, "\n");
}

/*
**	Line reader
**	This function reads the line until we get all necessary input
**	Getting Ctrl+D while getting input after wrong one(like only 1 ", \, etc...)
**	should not close the program, but is still wrong,
**	so we change it into Ctrl+C code.
**
**	Return value:
**		true	-	no EOF
**		false	-	we've encountered EOF as the first symbol in the line
*/

bool		lr_get_input(char **line)
{
	char				*temp;
	const char *const	ps2 = st_env_get_n_check("PS2", DEFAULT_PS2)->data;
	t_lr_ret_code		ret_code;

	temp = NULL;
	ft_strdel(line);
	ret_code = lr_get_line(line, true);
	while (ret_code == LR_N_LINE && needa_read_more(*line))
	{
		fizalize_the_line(line);
		ft_putstr(ps2);
		if ((ret_code = lr_get_line(&temp, false)) == LR_CTRL_D)
			ret_code = LR_CTRL_C;
		*line = ft_strjoin_ultimate(line, &temp);
	}
	if (ret_code == LR_CTRL_C)
		ft_strdel(line);
	else if (*line && **line)
		str_trim(line);
	return (ret_code != LR_CTRL_D);
}
