/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   defines.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/30 13:23:20 by dkovalch          #+#    #+#             */
/*   Updated: 2017/10/03 22:16:34 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DEFINES_H
# define DEFINES_H

/*
**	This file provides different useful defines.
**
**	Should be used:
**	everywhere it is needed.
*/

#ifndef WCOREDUMP
//# define WCOREDUMP(x)	(_W_INT(x) & WCOREFLAG)	//MAC
# define WCOREDUMP(x)	false
#endif

# define DEFAULT_SHELL_NAME		"21sh"
# define DEFAULT_PS2			"> "
# define DEFAULT_PROMPT			"%: "
# define DEFAULT_PROMPT_VAR		"PROMPT"
# define DEFAULT_PROMPT_CLR		"white"
# define DEFAULT_PROMPT_CLR_VAR	"PROMPT_CLR"

# define HISTORY_LIMIT			500

# ifdef DEBUG
#  undef DEFAULT_PROMPT
#  define DEFAULT_PROMPT		"[debug mode]%: "
#  undef DEFAULT_PROMPT_CLR
#  define DEFAULT_PROMPT_CLR	"green"
# endif

# define VERSION				"0.97"

#endif
