# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/05/11 20:28:25 by dkovalch          #+#    #+#              #
#    Updated: 2017/10/01 22:31:12 by dkovalch         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

								#################
								#	VARIABLES	#
								#################

NAME			= 21sh
NAME_D			= 21sh_deb

CC				= gcc

STD_FLAGS		= -x c -std=gnu11 -D _REENTRANT
ERR_FLAGS		= -Wall -Werror -Wextra -Wstrict-prototypes

COMPILE_FLAGS	= $(STD_FLAGS) $(ERR_FLAGS) -O3 -D NDEBUG
COMPILE_FLAGS_D	= $(STD_FLAGS) $(ERR_FLAGS) -O0 -D DEBUG -g -g3 $(SANITY_FLAGS)

LINKING_FLAGS	= $(ERR_FLAGS) -O3
LINKING_FLAGS_D	= $(ERR_FLAGS) -O0 -g -g3 $(SANITY_FLAGS)

LIBRARIES		= $(FT_PRINTF) $(LIBFT) -ltermcap
LIBRARIES_D		= $(FT_PRINTF_D) $(LIBFT_D) -ltermcap

SANITY_FLAGS	= -fno-omit-frame-pointer \
					-fsanitize=address,shift,null,signed-integer-overflow \
					-fsanitize=vla-bound,bool,enum,undefined

GCC7			:= $(shell command -v gcc-7 2> /dev/null)
ifdef GCC7
CC				= gcc-7
ERR_FLAGS	+= -Wnull-dereference
SANITY_FLAGS	+= -fsanitize=leak,bounds-strict
endif

INCLUDES		=\
	-iquote includes\
	-iquote $(SOURCES_FLD)/01_main\
	-iquote $(SOURCES_FLD)/02_help\
	-iquote $(SOURCES_FLD)/03_loop_runner\
	-iquote $(SOURCES_FLD)/04_prompt_builder\
	-iquote $(SOURCES_FLD)/05_line_reader\
	-iquote $(SOURCES_FLD)/06_key_actions\
	-iquote $(SOURCES_FLD)/07_cursor_movements\
	-iquote $(SOURCES_FLD)/08_line_processor\
	-iquote $(SOURCES_FLD)/09_command_processor\
	-iquote $(SOURCES_FLD)/10_signals\
	-iquote $(SOURCES_FLD)/11_helpers\
	-iquote $(SOURCES_FLD)/12_structs\
	-iquote $(FT_PRINTF_FLD) -iquote $(LIBFT_FLD)

PRINTF_2_ARGS	= @printf "%-15s %-65s\n"
PRINTF_3_ARGS	= @printf "%-15s %-65s%20s\n"


								#################
								#	LIBRARIES	#
								#################

LIBRARIES_FLD	= libraries

LIBFT_FLD		= $(LIBRARIES_FLD)/libft

LIBFT			= $(LIBFT_FLD)/libft.a
LIBFT_D			= $(LIBFT_FLD)/libftD.a

FT_PRINTF_FLD	= $(LIBRARIES_FLD)/ft_printf

FT_PRINTF		= $(FT_PRINTF_FLD)/libftprintf.a
FT_PRINTF_D		= $(FT_PRINTF_FLD)/libftprintfD.a


								#################
								#	SOURCES		#
								#################

SOURCES_FLD				= sources


VPATH	= $(SOURCES_FLD)/01_main
VPATH	+= :$(SOURCES_FLD)/02_help
VPATH	+= :$(SOURCES_FLD)/03_loop_runner
VPATH	+= :$(SOURCES_FLD)/04_prompt_builder
VPATH	+= :$(SOURCES_FLD)/05_line_reader
VPATH	+= :$(SOURCES_FLD)/06_key_actions
VPATH	+= :$(SOURCES_FLD)/07_cursor_movements
VPATH	+= :$(SOURCES_FLD)/08_line_processor
VPATH	+= :$(SOURCES_FLD)/09_command_processor
VPATH	+= :$(SOURCES_FLD)/10_signals
VPATH	+= :$(SOURCES_FLD)/11_helpers
VPATH	+= :$(SOURCES_FLD)/12_structs


								#################
								#	OBJECTS		#
								#################

OBJECTS_FLD = objects

OBJECTS =\
	$(OBJECTS_FLD)/main.o\
	$(OBJECTS_FLD)/initialization.o\
	$(OBJECTS_FLD)/help1.o\
	$(OBJECTS_FLD)/help2.o\
	$(OBJECTS_FLD)/loop_runner.o\
	$(OBJECTS_FLD)/prompt_builder.o\
	$(OBJECTS_FLD)/input_getter.o\
	$(OBJECTS_FLD)/line_getter.o\
	$(OBJECTS_FLD)/history_actions.o\
	$(OBJECTS_FLD)/key_logic.o\
	$(OBJECTS_FLD)/key_actions_basic.o\
	$(OBJECTS_FLD)/key_actions_arrows.o\
	$(OBJECTS_FLD)/key_actions_tab.o\
	$(OBJECTS_FLD)/key_actions_tab2.o\
	$(OBJECTS_FLD)/key_actions_tab3.o\
	$(OBJECTS_FLD)/key_actions_tab4.o\
	$(OBJECTS_FLD)/line_actions.o\
	$(OBJECTS_FLD)/cursor_clear.o\
	$(OBJECTS_FLD)/cursor_init.o\
	$(OBJECTS_FLD)/cursor_misc.o\
	$(OBJECTS_FLD)/cursor_logic.o\
	$(OBJECTS_FLD)/cursor_movements.o\
	$(OBJECTS_FLD)/stage1_cut.o\
	$(OBJECTS_FLD)/stage2_rearrange.o\
	$(OBJECTS_FLD)/stage3_analyze.o\
	$(OBJECTS_FLD)/stage3_process.o\
	$(OBJECTS_FLD)/stage4_additional.o\
	$(OBJECTS_FLD)/stage4_redirections_begin.o\
	$(OBJECTS_FLD)/stage4_redirections_end.o\
	$(OBJECTS_FLD)/stage4_redirections_helpers.o\
	$(OBJECTS_FLD)/stage4_variables.o\
	$(OBJECTS_FLD)/stage4_wildcard_begin.o\
	$(OBJECTS_FLD)/stage4_wildcard_end.o\
	$(OBJECTS_FLD)/buildin_cd.o\
	$(OBJECTS_FLD)/buildin_echo.o\
	$(OBJECTS_FLD)/buildin_env.o\
	$(OBJECTS_FLD)/buildin_history.o\
	$(OBJECTS_FLD)/stage1_execution.o\
	$(OBJECTS_FLD)/stage2_execution.o\
	$(OBJECTS_FLD)/stage2_heredoc.o\
	$(OBJECTS_FLD)/stage2_redirections.o\
	$(OBJECTS_FLD)/stage3_execution.o\
	$(OBJECTS_FLD)/stage4_child.o\
	$(OBJECTS_FLD)/stage4_parent.o\
	$(OBJECTS_FLD)/stage5_restore_fds.o\
	$(OBJECTS_FLD)/signals.o\
	$(OBJECTS_FLD)/error.o\
	$(OBJECTS_FLD)/grammar.o\
	$(OBJECTS_FLD)/grammar2.o\
	$(OBJECTS_FLD)/grammar3.o\
	$(OBJECTS_FLD)/grammar4.o\
	$(OBJECTS_FLD)/service.o\
	$(OBJECTS_FLD)/service2.o\
	$(OBJECTS_FLD)/string.o\
	$(OBJECTS_FLD)/string2.o\
	$(OBJECTS_FLD)/string_array.o\
	$(OBJECTS_FLD)/string_array2.o\
	$(OBJECTS_FLD)/children.o\
	$(OBJECTS_FLD)/command.o\
	$(OBJECTS_FLD)/environment.o\
	$(OBJECTS_FLD)/environment2.o\
	$(OBJECTS_FLD)/heredoc.o\
	$(OBJECTS_FLD)/history.o\
	$(OBJECTS_FLD)/history2.o\
	$(OBJECTS_FLD)/redirection.o



OBJECTS_D = $(patsubst %.o, %_D.o, $(OBJECTS))


								#############
								#	RULES	#
								#############

.SILENT :

all : $(NAME)

debug : $(NAME_D)

$(OBJECTS_FLD) :
	mkdir -p $(OBJECTS_FLD)

$(FT_PRINTF) :
	$(PRINTF_2_ARGS) "Making" "ft_printf"
	make -j -C $(FT_PRINTF_FLD)

$(LIBFT) :
	$(PRINTF_2_ARGS) "Making" "libft"
	make -j -C $(LIBFT_FLD)

$(FT_PRINTF_D) :
	$(PRINTF_2_ARGS) "Making" "ft_printf debug"
	make debug -j -C $(FT_PRINTF_FLD)

$(LIBFT_D) :
	$(PRINTF_2_ARGS) "Making" "libft debug"
	make debug -j -C $(LIBFT_FLD)

clean :
	rm -rf $(OBJECTS_FLD)
	make clean -C $(FT_PRINTF_FLD)
	make clean -C $(LIBFT_FLD)

fclean : clean
	rm -rf $(NAME) $(NAME_D) $(NAME).dSym $(NAME_D).dSym
	make fclean -C $(FT_PRINTF_FLD)
	make fclean -C $(LIBFT_FLD)

re : fclean all

dre : fclean debug

#########################
#	compilation rules	#
#########################

$(NAME) : $(LIBFT) $(FT_PRINTF) $(OBJECTS_FLD) $(OBJECTS)
	$(PRINTF_2_ARGS) "Linking" $(NAME)
	$(CC) $(INCLUDES) $(LINKING_FLAGS) $(OBJECTS) $(LIBRARIES) -o $(NAME)
	@echo "Finished!"

$(NAME_D) : $(LIBFT_D) $(FT_PRINTF_D) $(OBJECTS_FLD) $(OBJECTS_D)
	$(PRINTF_3_ARGS) "Linking" $(NAME_D) "with sanitizers"
	$(CC) $(INCLUDES) $(LINKING_FLAGS_D) $(OBJECTS_D) $(LIBRARIES_D) -o $(NAME_D)
	@echo "Finished!"

#####################
#	objects rules	#
#####################

$(OBJECTS_FLD)/%.o : %.c
	$(PRINTF_2_ARGS) "Compiling" $<
	$(CC) $(INCLUDES) $(COMPILE_FLAGS) -c $< -o $@

$(OBJECTS_FLD)/%_D.o : %.c
	$(PRINTF_3_ARGS) "Compiling" $< "with sanitizers"
	$(CC) $(INCLUDES) $(COMPILE_FLAGS_D) -c $< -o $@
